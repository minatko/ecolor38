function sliders() {
    $(".products-slider").slick({
        slidesToShow: 3,
        appendArrows: ".products-slider-btns",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    appendArrows: $(".products-slider")
                }
            }
        ]
    });
    $(".material-plus-slider").slick({
        slidesToShow: 4,
        slidesToScroll: 4,
        dots: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });
    $(".ekrany-slider").slick({
        slidesToShow: 8,
        responsive: [
            {
                breakpoint: 1260,
                settings: {
                    slidesToShow: 6
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 410,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $(".related-slider").slick({
        slidesToShow: 4
    });

    $(".diplomas-slider").slick({
        slidesToShow: 5,
        centerMode: true,
        centerPadding: "40px"
    });
}

function servicesHover() {
    var services = $(".services");
    var service = $(".service");

    services.height(services.height());

    $(window).resize(function() {
        services.removeAttr("style");
        services.height(services.height());
    });

    function serviceHoverIn(e) {
        var serv = $(e.currentTarget);
        serv.addClass("hover").addClass("shadow");

        var circs = serv.find(".circs");

        circs.dequeue().slideDown();
        /*
        if (circs.css("display") == "none") {
            circs.addClass("animating").slideDown(300, function() {
                $(this).removeClass("animating");
            });
        }*/
    }

    function serviceHoverOut(e) {
        var serv = $(e.currentTarget);
        serv.removeClass("shadow");

        var circs = serv.find(".circs");

        circs.dequeue().slideUp(300, function() {
            $(this).attr("style", "display:none");
            serv.removeClass("hover");
        });
        /*
        if (circs.css("display") != "none" && !circs.hasClass('animating')) {
            circs.addClass("animating").slideUp(300, function() {
                $(this).removeClass("animating");
            });
        }*/
    }

    service.hover(serviceHoverIn, serviceHoverOut);
}

function accordion() {
    /*
    $(".questions").easyResponsiveTabs({
        tabidentify: "questions-tabs",
        activetab_bg: "#37cfd8",
        inactive_bg: "#fff",
        activate: function(currentTab, e) {}
    });*/
    $(".questions").responsiveTabs({
        navigationContainer: ".questions-list",
        startCollapsed: "accordion"
    });
}

function results() {
    $(".cocoen").cocoen();

    $(".results-slider").slick({
        slidesToShow: 1,
        swipe: false,
        touchMove: false
    });
}

function headerMenu() {
    $(".header-menu-toggler").click(function(e) {
        e.preventDefault();

        $(this).toggleClass("opened");
        $(".header-menu-list").slideToggle();
    });
    $(window).resize(function() {
        $(".header-menu-toggler").removeClass("opened");
        $(".header-menu-list").removeAttr("style");
    });
}

function cartBtns() {
    $(document).on("click", ".minus", function(e) {
        var input = $(e.target).siblings("input");

        if (input.val() > 1) {
            input.val(input.val() - 1);
            input.change();
        }
    });
    $(document).on("click", ".plus", function(e) {
        var input = $(e.target).siblings("input");

        input.val(parseInt(input.val()) + 1);
        input.change();
    });
}

function contacts() {
    $(".contacts-map-controls a").click(function(e) {
        e.preventDefault();
        $(this)
            .siblings()
            .removeClass("active");
        $(this).addClass("active");

        $(".contacts-maps > div").css("opacity", 0);
        $($(this).attr("href")).css("opacity", 1);
    });
}

$(function() {
    sliders();
    servicesHover();
    accordion();
    results();
    headerMenu();
    cartBtns();
    contacts();

    $(document).on("af_complete", function(event, response) {
        if (response.success) {
            $.fancybox.close();
            $.fancybox.open({
                src: "#thank-you"
            });
        } else {
            $(".ajax-form-message").text(response.message);
            if (response.data.checkbox) {
                $("#modal-order-checkbox").addClass("error");
            }
        }
    });
});
