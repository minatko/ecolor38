<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 11,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Наш блог',
    'longtitle' => 'Блог о реставрации ванн от Эколор38',
    'description' => '',
    'alias' => '30-polezno',
    'alias_visible' => 1,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 1,
    'introtext' => '',
    'content' => '<div align="center">[[getResources? &amp;parents=`11` &amp;depth=`0` &amp;limit=`100` &amp;tpl=`blogtpl` &includeTVs=`blogimg` ]]</div>',
    'richtext' => 1,
    'template' => 2,
    'menuindex' => 10,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1484725872,
    'editedby' => 1,
    'editedon' => 1485335590,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1484725860,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => '30-polezno/',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'blog_img' => 
    array (
      0 => 'blog_img',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'desc' => 
    array (
      0 => 'desc',
      1 => 'На этой странице Вы можете прочитать полезные статьи о методах и ценах реставрации ванн от компании Эколор38.',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'titl' => 
    array (
      0 => 'titl',
      1 => 'Блог о реставрации ванн от Эколор38',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="https://ecolor38.ru/"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>Блог о реставрации ванн от Эколор38</title>
  <meta name="description" content="На этой странице Вы можете прочитать полезные статьи о методах и ценах реставрации ванн от компании Эколор38." />
<meta name="keywords" content="реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">
    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>
     <div id="top_nav" class="menu">
    <ul>
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul> </div>  

    <h1>Блог о реставрации ванн от Эколор38</h1>
<div class="content">
    <div align="center"><div class="mater">
<div class="materzag">Уголки для ванны: красиво и практично</div>
<div class="materimg"><img style="height: 200px;" src="/images/ugol-u24871-fr.jpg" alt="Уголки для ванны: красиво и практично" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Ванна должна быть не только привлекательной, но и практичной. Если раньше об этом люди не сильно беспокоились, то сегодня наличие уголков в ванной стало обязательным...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="50-ugolki-dlya-vanni.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Стакрил - отличный способ преобразить ванну</div>
<div class="materimg"><img style="height: 200px;" src="/images/emalirovka-vann-u24623-fr.jpg" alt="Стакрил - отличный способ преобразить ванну" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Обращали ли вы внимание на то, что все гости идут в ванну для того, чтобы вымыть руки? И если в ванной комнате не все в порядке, то вам будет не очень приятно пускать посторонних людей в ванну. Сколы и трещины в ванне рано или поздно появятся — это неизбежно...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="45-vosstanovlenie-stakrilom.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Ремонт ванны: дешево, но не сердито</div>
<div class="materimg"><img style="height: 200px;" src="/images/vanni-u24835-fr.jpg" alt="Ремонт ванны: дешево, но не сердито" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="138-remont-vanny-deshevo-no-ne-serdito.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Реставрация ванны. Менять зачем реставрировать: расставим знаки препинания.</div>
<div class="materimg"><img style="height: 200px;" src="/images/nalivnaya-vanna-u24832-fr.jpg" alt="Реставрация ванны. Менять зачем реставрировать: расставим знаки препинания." /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Ванна со временем утрачивает свой привлекательный внешний вид и требует ремонта или реставрации. Для этих целей можно воспользоваться разными методами. Очень популярна наливная ванна...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="restavraciya-vann.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Почему преображение вашей ванны стоит заказывать у нас?</div>
<div class="materimg"><img style="height: 200px;" src="/images/red-faq.jpg" alt="Почему преображение вашей ванны стоит заказывать у нас?" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Что такое вкладыш для ванны и чем он хорош? Это специальная форма из пластика, которая полностью повторяет форму вашей ванны. Его толщина достигает 6 миллиметров, так что объем и размеры остаются прежними. Меняется только внешний вид и свойства...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="52-glavnaya.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Как правильно покрасить ванну?</div>
<div class="materimg"><img style="height: 200px;" src="/images/pokraska-u24742-fr.jpg" alt="Как правильно покрасить ванну?" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Трещины, сколы, потертости, разводы от воды, желтизна и известковый налет, рано или поздно, превращают новую ванну в непривлекательную. Такой ванной нет никакого удовольствия пользоваться...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="47-kak-pokrasit-vannu.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Жидкий стакрил: ваш надежный помощник в обновлении ванны</div>
<div class="materimg"><img style="height: 200px;" src="/images/stacril.jpg" alt="Жидкий стакрил: ваш надежный помощник в обновлении ванны" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="46-zidkiy-akril.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Акриловый вкладыш для ванны: и ванна, как новая!</div>
<div class="materimg"><img style="height: 200px;" src="/images/akrilvkladish-u24653-fr.jpg" alt="Акриловый вкладыш для ванны: и ванна, как новая!" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Что такое вкладыш для ванны и чем он хорош? Это специальная форма из пластика, которая полностью повторяет форму вашей ванны. Его толщина достигает 6 миллиметров, так что объем и размеры остаются прежними. Меняется только внешний вид и свойства...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="48-akriloviy-vkladish.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Восстановление ванны: не надо менять, все можно исправить!</div>
<div class="materimg"><img style="height: 200px;" src="/images/restavriruem-vann.jpg" alt="Восстановление ванны: не надо менять, все можно исправить!" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Трещины, сколы, потертости, разводы от воды, желтизна и известковый налет, рано или поздно, превращают новую ванну в непривлекательную. Такой ванной нет никакого удовольствия пользоваться...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="44-vosstanovlenie-emali-vanni.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Виды ванн и методы реставрации</div>
<div class="materimg"><img style="height: 200px;" src="/images/emalirovka-vann-u24623-fr.jpg" alt="Виды ванн и методы реставрации" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="65-vidi-vann.html">Читать дальше</a></div>
</div></div>
            </div>
          <div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта https://ecolor38.ru/`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="30-polezno/" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	
</div>
</div>   
</div>
   </body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[*titl]]' => 'Блог о реставрации ванн от Эколор38',
    '[[*desc]]' => 'На этой странице Вы можете прочитать полезные статьи о методах и ценах реставрации ванн от компании Эколор38.',
    '[[*keyw]]' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="https://ecolor38.ru/"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>Блог о реставрации ванн от Эколор38</title>
  <meta name="description" content="На этой странице Вы можете прочитать полезные статьи о методах и ценах реставрации ванн от компании Эколор38." />
<meta name="keywords" content="реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
    '[[$header]]' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
    '[[*id]]' => 11,
    '[[~3]]' => 'o-nas.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="o-nas.html">О нас</a></li>`&else=`<li><a href="o-nas.html">О нас</a></li>`]]' => '<li><a href="o-nas.html">О нас</a></li>',
    '[[~4]]' => 'nashi-uslugi.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="nashi-uslugi.html">Услуги</a></li>`&else=`<li><a href="nashi-uslugi.html">Услуги</a></li>`]]' => '<li><a href="nashi-uslugi.html">Услуги</a></li>',
    '[[~5]]' => 'ekrany-pod-vannu-i-dushevye-poddony.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>`&else=`<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>`]]' => '<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>',
    '[[~6]]' => 'tseny-na-uslugi.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="tseny-na-uslugi.html">Цены</a></li>`&else=`<li><a href="tseny-na-uslugi.html">Цены</a></li>`]]' => '<li><a href="tseny-na-uslugi.html">Цены</a></li>',
    '[[~7]]' => 'aktsiya.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="aktsiya.html">Акции</a></li>`&else=`<li><a href="aktsiya.html">Акции</a></li>`]]' => '<li><a href="aktsiya.html">Акции</a></li>',
    '[[~8]]' => 'nashi-materialy.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="nashi-materialy.html">Материалы</a></li>`&else=`<li><a href="nashi-materialy.html">Материалы</a></li>`]]' => '<li><a href="nashi-materialy.html">Материалы</a></li>',
    '[[~9]]' => 'akrilovie-vkladishi.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>`&else=`<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>`]]' => '<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>',
    '[[~10]]' => 'otzyvy-klientov.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="otzyvy-klientov.html">Отзывы</a></li>`&else=`<li><a href="otzyvy-klientov.html">Отзывы</a></li>`]]' => '<li><a href="otzyvy-klientov.html">Отзывы</a></li>',
    '[[~11]]' => '30-polezno/',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="30-polezno/">Наш блог</a></li>`&else=`<li><a href="30-polezno/">Наш блог</a></li>`]]' => '<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="30-polezno/">Наш блог</a></li>',
    '[[~12]]' => 'kontakti.html',
    '[[If?   &subject=`11`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="kontakti.html">Контакты</a></li>`&else=`<li><a href="kontakti.html">Контакты</a></li>`]]' => '<li><a href="kontakti.html">Контакты</a></li>',
    '[[$menu]]' => ' <div id="top_nav" class="menu">
    <ul>
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul> </div>  
',
    '[[*longtitle]]' => 'Блог о реставрации ванн от Эколор38',
    '[[getResources? &amp;parents=`11` &amp;depth=`0` &amp;limit=`100` &amp;tpl=`blogtpl` &includeTVs=`blogimg` ]]' => '<div class="mater">
<div class="materzag">Уголки для ванны: красиво и практично</div>
<div class="materimg"><img style="height: 200px;" src="/images/ugol-u24871-fr.jpg" alt="Уголки для ванны: красиво и практично" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Ванна должна быть не только привлекательной, но и практичной. Если раньше об этом люди не сильно беспокоились, то сегодня наличие уголков в ванной стало обязательным...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="50-ugolki-dlya-vanni.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Стакрил - отличный способ преобразить ванну</div>
<div class="materimg"><img style="height: 200px;" src="/images/emalirovka-vann-u24623-fr.jpg" alt="Стакрил - отличный способ преобразить ванну" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Обращали ли вы внимание на то, что все гости идут в ванну для того, чтобы вымыть руки? И если в ванной комнате не все в порядке, то вам будет не очень приятно пускать посторонних людей в ванну. Сколы и трещины в ванне рано или поздно появятся — это неизбежно...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="45-vosstanovlenie-stakrilom.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Ремонт ванны: дешево, но не сердито</div>
<div class="materimg"><img style="height: 200px;" src="/images/vanni-u24835-fr.jpg" alt="Ремонт ванны: дешево, но не сердито" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="138-remont-vanny-deshevo-no-ne-serdito.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Реставрация ванны. Менять зачем реставрировать: расставим знаки препинания.</div>
<div class="materimg"><img style="height: 200px;" src="/images/nalivnaya-vanna-u24832-fr.jpg" alt="Реставрация ванны. Менять зачем реставрировать: расставим знаки препинания." /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Ванна со временем утрачивает свой привлекательный внешний вид и требует ремонта или реставрации. Для этих целей можно воспользоваться разными методами. Очень популярна наливная ванна...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="restavraciya-vann.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Почему преображение вашей ванны стоит заказывать у нас?</div>
<div class="materimg"><img style="height: 200px;" src="/images/red-faq.jpg" alt="Почему преображение вашей ванны стоит заказывать у нас?" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Что такое вкладыш для ванны и чем он хорош? Это специальная форма из пластика, которая полностью повторяет форму вашей ванны. Его толщина достигает 6 миллиметров, так что объем и размеры остаются прежними. Меняется только внешний вид и свойства...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="52-glavnaya.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Как правильно покрасить ванну?</div>
<div class="materimg"><img style="height: 200px;" src="/images/pokraska-u24742-fr.jpg" alt="Как правильно покрасить ванну?" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Трещины, сколы, потертости, разводы от воды, желтизна и известковый налет, рано или поздно, превращают новую ванну в непривлекательную. Такой ванной нет никакого удовольствия пользоваться...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="47-kak-pokrasit-vannu.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Жидкий стакрил: ваш надежный помощник в обновлении ванны</div>
<div class="materimg"><img style="height: 200px;" src="/images/stacril.jpg" alt="Жидкий стакрил: ваш надежный помощник в обновлении ванны" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="46-zidkiy-akril.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Акриловый вкладыш для ванны: и ванна, как новая!</div>
<div class="materimg"><img style="height: 200px;" src="/images/akrilvkladish-u24653-fr.jpg" alt="Акриловый вкладыш для ванны: и ванна, как новая!" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Что такое вкладыш для ванны и чем он хорош? Это специальная форма из пластика, которая полностью повторяет форму вашей ванны. Его толщина достигает 6 миллиметров, так что объем и размеры остаются прежними. Меняется только внешний вид и свойства...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="48-akriloviy-vkladish.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Восстановление ванны: не надо менять, все можно исправить!</div>
<div class="materimg"><img style="height: 200px;" src="/images/restavriruem-vann.jpg" alt="Восстановление ванны: не надо менять, все можно исправить!" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">Трещины, сколы, потертости, разводы от воды, желтизна и известковый налет, рано или поздно, превращают новую ванну в непривлекательную. Такой ванной нет никакого удовольствия пользоваться...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="44-vosstanovlenie-emali-vanni.html">Читать дальше</a></div>
</div>
<div class="mater">
<div class="materzag">Виды ванн и методы реставрации</div>
<div class="materimg"><img style="height: 200px;" src="/images/emalirovka-vann-u24623-fr.jpg" alt="Виды ванн и методы реставрации" /></div>
<div class="materdesc" style="     min-height: 140px;   line-height: 16px;
    font-size: 15px;
    text-align: justify;">На современном рынке ванны представлены в самом широком ассортименте – по дизайну, «функционалу» и цене. Что касается материалов изготовления, то это чугун, сталь и акрил – именно от этого и зависят эксплуатационные характеристики ванны...</div>
<div class="materbut"><a style="display: block; color: #fff; text-decoration: none;" href="65-vidi-vann.html">Читать дальше</a></div>
</div>',
    '[[$footer]]' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта https://ecolor38.ru/`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="30-polezno/" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="[[++site_url]]"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>[[*titl]]</title>
  <meta name="description" content="[[*desc]]" />
<meta name="keywords" content="[[*keyw]]" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="[[++site_url]]"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>[[*titl]]</title>
  <meta name="description" content="[[*desc]]" />
<meta name="keywords" content="[[*keyw]]" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'menu' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'menu',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => ' <div id="top_nav" class="menu">
    <ul>
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~3]]">О нас</a></li>`&else=`<li><a href="[[~3]]">О нас</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~4]]">Услуги</a></li>`&else=`<li><a href="[[~4]]">Услуги</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~5]]">Экраны</a></li>`&else=`<li><a href="[[~5]]">Экраны</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~6]]">Цены</a></li>`&else=`<li><a href="[[~6]]">Цены</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~7]]">Акции</a></li>`&else=`<li><a href="[[~7]]">Акции</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~8]]">Материалы</a></li>`&else=`<li><a href="[[~8]]">Материалы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~9]]">Акриловые вкладыши</a></li>`&else=`<li><a href="[[~9]]">Акриловые вкладыши</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~10]]">Отзывы</a></li>`&else=`<li><a href="[[~10]]">Отзывы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~11]]">Наш блог</a></li>`&else=`<li><a href="[[~11]]">Наш блог</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~12]]">Контакты</a></li>`&else=`<li><a href="[[~12]]">Контакты</a></li>`]]
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="[[~3]]">О нас</a></li>
<li><a href="[[~4]]">Услуги</a></li>
<li><a href="[[~5]]">Экраны</a></li>
<li><a href="[[~6]]">Цены</a></li>
<li><a href="[[~7]]">Акции</a></li>
<li><a href="[[~8]]">Материалы</a></li>
<li><a href="[[~9]]">Акриловые вкладыши</a></li>
<li><a href="[[~10]]">Отзывы</a></li>
<li><a href="[[~11]]">Наш блог</a></li>
<li><a href="[[~12]]">Контакты</a></li>
    </ul> </div>  
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => ' <div id="top_nav" class="menu">
    <ul>
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~3]]">О нас</a></li>`&else=`<li><a href="[[~3]]">О нас</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~4]]">Услуги</a></li>`&else=`<li><a href="[[~4]]">Услуги</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~5]]">Экраны</a></li>`&else=`<li><a href="[[~5]]">Экраны</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~6]]">Цены</a></li>`&else=`<li><a href="[[~6]]">Цены</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~7]]">Акции</a></li>`&else=`<li><a href="[[~7]]">Акции</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~8]]">Материалы</a></li>`&else=`<li><a href="[[~8]]">Материалы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~9]]">Акриловые вкладыши</a></li>`&else=`<li><a href="[[~9]]">Акриловые вкладыши</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~10]]">Отзывы</a></li>`&else=`<li><a href="[[~10]]">Отзывы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~11]]">Наш блог</a></li>`&else=`<li><a href="[[~11]]">Наш блог</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~12]]">Контакты</a></li>`&else=`<li><a href="[[~12]]">Контакты</a></li>`]]
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="[[~3]]">О нас</a></li>
<li><a href="[[~4]]">Услуги</a></li>
<li><a href="[[~5]]">Экраны</a></li>
<li><a href="[[~6]]">Цены</a></li>
<li><a href="[[~7]]">Акции</a></li>
<li><a href="[[~8]]">Материалы</a></li>
<li><a href="[[~9]]">Акриловые вкладыши</a></li>
<li><a href="[[~10]]">Отзывы</a></li>
<li><a href="[[~11]]">Наш блог</a></li>
<li><a href="[[~12]]">Контакты</a></li>
    </ul> </div>  
',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта [[++site_url]]`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="[[~[[*id]]]]" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта [[++site_url]]`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="[[~[[*id]]]]" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'If' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'If',
          'description' => 'Simple if (conditional) snippet',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'subject' => 
            array (
              'name' => 'subject',
              'desc' => 'The data being affected.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The data being affected.',
              'area_trans' => '',
            ),
            'operator' => 
            array (
              'name' => 'operator',
              'desc' => 'The type of conditional.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'EQ',
                  'text' => 'EQ',
                  'name' => 'EQ',
                ),
                1 => 
                array (
                  'value' => 'NEQ',
                  'text' => 'NEQ',
                  'name' => 'NEQ',
                ),
                2 => 
                array (
                  'value' => 'LT',
                  'text' => 'LT',
                  'name' => 'LT',
                ),
                3 => 
                array (
                  'value' => 'GT',
                  'text' => 'GT',
                  'name' => 'GT',
                ),
                4 => 
                array (
                  'value' => 'LTE',
                  'text' => 'LTE',
                  'name' => 'LTE',
                ),
                5 => 
                array (
                  'value' => 'GT',
                  'text' => 'GTE',
                  'name' => 'GTE',
                ),
                6 => 
                array (
                  'value' => 'EMPTY',
                  'text' => 'EMPTY',
                  'name' => 'EMPTY',
                ),
                7 => 
                array (
                  'value' => 'NOTEMPTY',
                  'text' => 'NOTEMPTY',
                  'name' => 'NOTEMPTY',
                ),
                8 => 
                array (
                  'value' => 'ISNULL',
                  'text' => 'ISNULL',
                  'name' => 'ISNULL',
                ),
                9 => 
                array (
                  'value' => 'inarray',
                  'text' => 'INARRAY',
                  'name' => 'INARRAY',
                ),
              ),
              'value' => 'EQ',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The type of conditional.',
              'area_trans' => '',
            ),
            'operand' => 
            array (
              'name' => 'operand',
              'desc' => 'When comparing to the subject, this is the data to compare to.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'When comparing to the subject, this is the data to compare to.',
              'area_trans' => '',
            ),
            'then' => 
            array (
              'name' => 'then',
              'desc' => 'If conditional was successful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was successful, output this.',
              'area_trans' => '',
            ),
            'else' => 
            array (
              'name' => 'else',
              'desc' => 'If conditional was unsuccessful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was unsuccessful, output this.',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'getResources' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getResources',
          'description' => '<strong>1.6.1-pl</strong> A general purpose Resource listing and summarization snippet for MODX Revolution',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * getResources
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Jason Coward
 * @copyright Copyright 2010-2013, Jason Coward
 *
 * TEMPLATES
 *
 * tpl - Name of a chunk serving as a resource template
 * [NOTE: if not provided, properties are dumped to output for each resource]
 *
 * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value
 * (see idx property)
 * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first
 * property)
 * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last
 * property)
 * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource
 *
 * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the
 * conditionalTpls property. Must be a resource field; does not work with Template Variables.
 * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to
 * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,
 * and tpl_{n} will take precedence over any defined conditionalTpls]
 *
 * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output
 * [NOTE: Does not work with toSeparatePlaceholders]
 *
 * SELECTION
 *
 * parents - Comma-delimited list of ids serving as parents
 *
 * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified
 * parents will be used (all contexts if 0 is specified) [default=]
 *
 * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]
 *
 * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two
 * delimiters and two value search formats. The first delimiter || represents a logical OR and the
 * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.
 * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the
 * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An
 * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`
 * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]
 * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value
 * specifically set for the Resource and it is not evaluated.]
 *
 * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to
 * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`
 * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]
 *
 * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to
 * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`
 * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]
 *
 * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be
 * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`
 *
 * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]
 * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]
 * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]
 * sortbyAlias - (Opt) Query alias for sortby field [default=]
 * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]
 * sortdir - (Opt) Order which to sort by [default=DESC]
 * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]
 * limit - (Opt) Limits the number of resources returned [default=5]
 * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]
 * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set
 * according to cache settings, any other integer value = number of seconds to cache result set [default=0]
 *
 * OPTIONS
 *
 * includeContent - (Opt) Indicates if the content of each resource should be returned in the
 * results [default=0]
 * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available
 * to each resource template [default=0]
 * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified
 * by name in a comma-delimited list [default=]
 * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]
 * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited
 * list [default=]
 * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the
 * resource being summarized [default=0]
 * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified
 * by name in a comma-delimited list [default=]
 * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]
 * idx - (Opt) You can define the starting idx of the resources, which is an property that is
 * incremented as each resource is rendered [default=1]
 * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]
 * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of
 * resources being summarized + first - 1]
 * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]
 * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]
 *
 */
$output = array();
$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";

/* set default properties */
$tpl = !empty($tpl) ? $tpl : \'\';
$includeContent = !empty($includeContent) ? true : false;
$includeTVs = !empty($includeTVs) ? true : false;
$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();
$processTVs = !empty($processTVs) ? true : false;
$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();
$prepareTVs = !empty($prepareTVs) ? true : false;
$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();
$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';
$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));
array_walk($parents, \'trim\');
$parents = array_unique($parents);
$depth = isset($depth) ? (integer) $depth : 10;

$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';
$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';
$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();

$where = !empty($where) ? $modx->fromJSON($where) : array();
$showUnpublished = !empty($showUnpublished) ? true : false;
$showDeleted = !empty($showDeleted) ? true : false;

$sortby = isset($sortby) ? $sortby : \'publishedon\';
$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';
$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';
$sortbyEscaped = !empty($sortbyEscaped) ? true : false;
$sortdir = isset($sortdir) ? $sortdir : \'DESC\';
$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';
$limit = isset($limit) ? (integer) $limit : 5;
$offset = isset($offset) ? (integer) $offset : 0;
$totalVar = !empty($totalVar) ? $totalVar : \'total\';

$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;
if (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {
    if ($dbCacheFlag == \'0\') {
        $dbCacheFlag = false;
    } elseif ($dbCacheFlag == \'1\') {
        $dbCacheFlag = true;
    } else {
        $dbCacheFlag = (integer) $dbCacheFlag;
    }
}

/* multiple context support */
$contextArray = array();
$contextSpecified = false;
if (!empty($context)) {
    $contextArray = explode(\',\',$context);
    array_walk($contextArray, \'trim\');
    $contexts = array();
    foreach ($contextArray as $ctx) {
        $contexts[] = $modx->quote($ctx);
    }
    $context = implode(\',\',$contexts);
    $contextSpecified = true;
    unset($contexts,$ctx);
} else {
    $context = $modx->quote($modx->context->get(\'key\'));
}

$pcMap = array();
$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);
$pcQuery->select(array(\'id\', \'context_key\'));
if ($pcQuery->prepare() && $pcQuery->stmt->execute()) {
    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {
        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];
    }
}

$children = array();
$parentArray = array();
foreach ($parents as $parent) {
    $parent = (integer) $parent;
    if ($parent === 0) {
        $pchildren = array();
        if ($contextSpecified) {
            foreach ($contextArray as $pCtx) {
                if (!in_array($pCtx, $contextArray)) {
                    continue;
                }
                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                $pcchildren = $modx->getChildIds($parent, $depth, $options);
                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
            }
        } else {
            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));
            $cQuery->select(array(\'key\'));
            if ($cQuery->prepare() && $cQuery->stmt->execute()) {
                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {
                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                    $pcchildren = $modx->getChildIds($parent, $depth, $options);
                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
                }
            }
        }
        $parentArray[] = $parent;
    } else {
        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;
        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");
        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {
            $parent = next($parents);
            continue;
        }
        $parentArray[] = $parent;
        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();
        $pchildren = $modx->getChildIds($parent, $depth, $options);
    }
    if (!empty($pchildren)) $children = array_merge($children, $pchildren);
    $parent = next($parents);
}
$parents = array_merge($parentArray, $children);

/* build query */
$criteria = array("modResource.parent IN (" . implode(\',\', $parents) . ")");
if ($contextSpecified) {
    $contextResourceTbl = $modx->getTableName(\'modContextResource\');
    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";
}
if (empty($showDeleted)) {
    $criteria[\'deleted\'] = \'0\';
}
if (empty($showUnpublished)) {
    $criteria[\'published\'] = \'1\';
}
if (empty($showHidden)) {
    $criteria[\'hidemenu\'] = \'0\';
}
if (!empty($hideContainers)) {
    $criteria[\'isfolder\'] = \'0\';
}
$criteria = $modx->newQuery(\'modResource\', $criteria);
if (!empty($tvFilters)) {
    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');
    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');
    $conditions = array();
    $operators = array(
        \'<=>\' => \'<=>\',
        \'===\' => \'=\',
        \'!==\' => \'!=\',
        \'<>\' => \'<>\',
        \'==\' => \'LIKE\',
        \'!=\' => \'NOT LIKE\',
        \'<<\' => \'<\',
        \'<=\' => \'<=\',
        \'=<\' => \'=<\',
        \'>>\' => \'>\',
        \'>=\' => \'>=\',
        \'=>\' => \'=>\'
    );
    foreach ($tvFilters as $fGroup => $tvFilter) {
        $filterGroup = array();
        $filters = explode($tvFiltersAndDelimiter, $tvFilter);
        $multiple = count($filters) > 0;
        foreach ($filters as $filter) {
            $operator = \'==\';
            $sqlOperator = \'LIKE\';
            foreach ($operators as $op => $opSymbol) {
                if (strpos($filter, $op, 1) !== false) {
                    $operator = $op;
                    $sqlOperator = $opSymbol;
                    break;
                }
            }
            $tvValueField = \'tvr.value\';
            $tvDefaultField = \'tv.default_text\';
            $f = explode($operator, $filter);
            if (count($f) >= 2) {
                if (count($f) > 2) {
                    $k = array_shift($f);
                    $b = join($operator, $f);
                    $f = array($k, $b);
                }
                $tvName = $modx->quote($f[0]);
                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {
                    $tvValue = $f[1];
                    if ($f[1] == (integer)$f[1]) {
                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";
                    } else {
                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";
                    }
                } else {
                    $tvValue = $modx->quote($f[1]);
                }
                if ($multiple) {
                    $filterGroup[] =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                } else {
                    $filterGroup =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                }
            } elseif (count($f) == 1) {
                $tvValue = $modx->quote($f[0]);
                if ($multiple) {
                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                } else {
                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                }
            }
        }
        $conditions[] = $filterGroup;
    }
    if (!empty($conditions)) {
        $firstGroup = true;
        foreach ($conditions as $cGroup => $c) {
            if (is_array($c)) {
                $first = true;
                foreach ($c as $cond) {
                    if ($first && !$firstGroup) {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);
                    } else {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);
                    }
                    $first = false;
                }
            } else {
                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);
            }
            $firstGroup = false;
        }
    }
}
/* include/exclude resources, via &resources=`123,-456` prop */
if (!empty($resources)) {
    $resourceConditions = array();
    $resources = explode(\',\',$resources);
    $include = array();
    $exclude = array();
    foreach ($resources as $resource) {
        $resource = (int)$resource;
        if ($resource == 0) continue;
        if ($resource < 0) {
            $exclude[] = abs($resource);
        } else {
            $include[] = $resource;
        }
    }
    if (!empty($include)) {
        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);
    }
    if (!empty($exclude)) {
        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);
    }
}
if (!empty($where)) {
    $criteria->where($where);
}

$total = $modx->getCount(\'modResource\', $criteria);
$modx->setPlaceholder($totalVar, $total);

$fields = array_keys($modx->getFields(\'modResource\'));
if (empty($includeContent)) {
    $fields = array_diff($fields, array(\'content\'));
}
$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);
$criteria->select($columns);
if (!empty($sortbyTV)) {
    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(
        "tvDefault.name" => $sortbyTV
    ));
    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(
        "tvSort.contentid = modResource.id",
        "tvSort.tmplvarid = tvDefault.id"
    ));
    if (empty($sortbyTVType)) $sortbyTVType = \'string\';
    if ($modx->getOption(\'dbtype\') === \'mysql\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    }
    $criteria->sortby("sortTV", $sortdirTV);
}
if (!empty($sortby)) {
    if (strpos($sortby, \'{\') === 0) {
        $sorts = $modx->fromJSON($sortby);
    } else {
        $sorts = array($sortby => $sortdir);
    }
    if (is_array($sorts)) {
        while (list($sort, $dir) = each($sorts)) {
            if ($sortbyEscaped) $sort = $modx->escape($sort);
            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";
            $criteria->sortby($sort, $dir);
        }
    }
}
if (!empty($limit)) $criteria->limit($limit, $offset);

if (!empty($debug)) {
    $criteria->prepare();
    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());
}
$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);

$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;
$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;
$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;

/* include parseTpl */
include_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';

$templateVars = array();
if (!empty($includeTVs) && !empty($includeTVList)) {
    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));
}
/** @var modResource $resource */
foreach ($collection as $resourceId => $resource) {
    $tvs = array();
    if (!empty($includeTVs)) {
        if (empty($includeTVList)) {
            $templateVars = $resource->getMany(\'TemplateVars\');
        }
        /** @var modTemplateVar $templateVar */
        foreach ($templateVars as $tvId => $templateVar) {
            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;
            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));
            } else {
                $value = $templateVar->getValue($resource->get(\'id\'));
                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {
                    $value = $templateVar->prepareOutput($value);
                }
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;
            }
        }
    }
    $odd = ($idx & 1);
    $properties = array_merge(
        $scriptProperties
        ,array(
            \'idx\' => $idx
            ,\'first\' => $first
            ,\'last\' => $last
            ,\'odd\' => $odd
        )
        ,$includeContent ? $resource->toArray() : $resource->get($fields)
        ,$tvs
    );
    $resourceTpl = false;
    if ($idx == $first && !empty($tplFirst)) {
        $resourceTpl = parseTpl($tplFirst, $properties);
    }
    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {
        $resourceTpl = parseTpl($tplLast, $properties);
    }
    $tplidx = \'tpl_\' . $idx;
    if (empty($resourceTpl) && !empty($$tplidx)) {
        $resourceTpl = parseTpl($$tplidx, $properties);
    }
    if ($idx > 1 && empty($resourceTpl)) {
        $divisors = getDivisors($idx);
        if (!empty($divisors)) {
            foreach ($divisors as $divisor) {
                $tplnth = \'tpl_n\' . $divisor;
                if (!empty($$tplnth)) {
                    $resourceTpl = parseTpl($$tplnth, $properties);
                    if (!empty($resourceTpl)) {
                        break;
                    }
                }
            }
        }
    }
    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {
        $resourceTpl = parseTpl($tplOdd, $properties);
    }
    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {
        $conTpls = $modx->fromJSON($conditionalTpls);
        $subject = $properties[$tplCondition];
        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';
        $tplOperator = strtolower($tplOperator);
        $tplCon = \'\';
        foreach ($conTpls as $operand => $conditionalTpl) {
            switch ($tplOperator) {
                case \'!=\':
                case \'neq\':
                case \'not\':
                case \'isnot\':
                case \'isnt\':
                case \'unequal\':
                case \'notequal\':
                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<\':
                case \'lt\':
                case \'less\':
                case \'lessthan\':
                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>\':
                case \'gt\':
                case \'greater\':
                case \'greaterthan\':
                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<=\':
                case \'lte\':
                case \'lessthanequals\':
                case \'lessthanorequalto\':
                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>=\':
                case \'gte\':
                case \'greaterthanequals\':
                case \'greaterthanequalto\':
                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'isempty\':
                case \'empty\':
                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;
                    break;
                case \'!empty\':
                case \'notempty\':
                case \'isnotempty\':
                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;
                    break;
                case \'isnull\':
                case \'null\':
                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;
                    break;
                case \'inarray\':
                case \'in_array\':
                case \'ia\':
                    $operand = explode(\',\', $operand);
                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;
                    break;
                case \'between\':
                case \'range\':
                case \'>=<\':
                case \'><\':
                    $operand = explode(\',\', $operand);
                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;
                    break;
                case \'==\':
                case \'=\':
                case \'eq\':
                case \'is\':
                case \'equal\':
                case \'equals\':
                case \'equalto\':
                default:
                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);
                    break;
            }
        }
        if (!empty($tplCon)) {
            $resourceTpl = parseTpl($tplCon, $properties);
        }
    }
    if (!empty($tpl) && empty($resourceTpl)) {
        $resourceTpl = parseTpl($tpl, $properties);
    }
    if ($resourceTpl === false && !empty($debug)) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');
    } else {
        $output[]= $resourceTpl;
    }
    $idx++;
}

/* output */
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
if (!empty($toSeparatePlaceholders)) {
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}

$output = implode($outputSeparator, $output);

$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);
$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);
if (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {
    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));
}

$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
    return \'\';
}
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as a resource template. NOTE: if not provided, properties are dumped to output for each resource.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'Name of a chunk serving as resource template for resources with an odd idx value (see idx property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for resources with an odd idx value (see idx property).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'Name of a chunk serving as resource template for the first resource (see first property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for the first resource (see first property).',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'Name of a chunk serving as resource template for the last resource (see last property).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as resource template for the last resource (see last property).',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a chunk serving as wrapper template for the Snippet output. This does not work with toSeparatePlaceholders.',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if empty output should be wrapped by the tplWrapper, if specified. Defaults to false.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.',
              'type' => 'textfield',
              'options' => '',
              'value' => 'publishedon',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A field name to sort by or JSON object of field names and sortdir for each field, e.g. {"publishedon":"ASC","createdon":"DESC"}. Defaults to publishedon.',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'Name of a Template Variable to sort by. Defaults to empty string.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Name of a Template Variable to sort by. Defaults to empty string.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'An optional type to indicate how to sort on the Template Variable value.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'string',
                  'value' => 'string',
                  'name' => 'Строка',
                ),
                1 => 
                array (
                  'text' => 'integer',
                  'value' => 'integer',
                  'name' => 'integer',
                ),
                2 => 
                array (
                  'text' => 'decimal',
                  'value' => 'decimal',
                  'name' => 'decimal',
                ),
                3 => 
                array (
                  'text' => 'datetime',
                  'value' => 'datetime',
                  'name' => 'datetime',
                ),
              ),
              'value' => 'string',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An optional type to indicate how to sort on the Template Variable value.',
              'area_trans' => '',
            ),
            'sortbyAlias' => 
            array (
              'name' => 'sortbyAlias',
              'desc' => 'Query alias for sortby field. Defaults to an empty string.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Query alias for sortby field. Defaults to an empty string.',
              'area_trans' => '',
            ),
            'sortbyEscaped' => 
            array (
              'name' => 'sortbyEscaped',
              'desc' => 'Determines if the field name specified in sortby should be escaped. Defaults to 0.',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Determines if the field name specified in sortby should be escaped. Defaults to 0.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'Order which to sort by. Defaults to DESC.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Order which to sort by. Defaults to DESC.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'Order which to sort a Template Variable by. Defaults to DESC.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Order which to sort a Template Variable by. Defaults to DESC.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'Limits the number of resources returned. Defaults to 5.',
              'type' => 'textfield',
              'options' => '',
              'value' => '5',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits the number of resources returned. Defaults to 5.',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'An offset of resources returned by the criteria to skip.',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An offset of resources returned by the criteria to skip.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Delimited-list of TemplateVar values to filter resources by. Supports two delimiters and two value search formats. THe first delimiter || represents a logical OR and the primary grouping mechanism.  Within each group you can provide a comma-delimited list of values. These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the value, indicating you are searching for the value in any TemplateVar tied to the Resource. An example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`. <br />NOTE: filtering by values uses a LIKE query and % is considered a wildcard. <br />ANOTHER NOTE: This only looks at the raw value set for specific Resource, i. e. there must be a value specifically set for the Resource and it is not evaluated.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'The delimiter to use to separate logical AND expressions in tvFilters. Default is ,',
              'type' => 'textfield',
              'options' => '',
              'value' => ',',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The delimiter to use to separate logical AND expressions in tvFilters. Default is ,',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'The delimiter to use to separate logical OR expressions in tvFilters. Default is ||',
              'type' => 'textfield',
              'options' => '',
              'value' => '||',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The delimiter to use to separate logical OR expressions in tvFilters. Default is ||',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'Integer value indicating depth to search for resources from each parent. Defaults to 10.',
              'type' => 'textfield',
              'options' => '',
              'value' => '10',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Integer value indicating depth to search for resources from each parent. Defaults to 10.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'Optional. Comma-delimited list of ids serving as parents.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Optional. Comma-delimited list of ids serving as parents.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'Indicates if the content of each resource should be returned in the results. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if the content of each resource should be returned in the results. Defaults to false.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values should be included in the properties available to each resource template. Defaults to false.',
              'area_trans' => '',
            ),
            'includeTVList' => 
            array (
              'name' => 'includeTVList',
              'desc' => 'Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits included TVs to those specified as a comma-delimited list of TV names. Defaults to empty.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'Indicates if Resources that are hidden from menus should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are hidden from menus should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'Indicates if Resources that are unpublished should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are unpublished should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'Indicates if Resources that are deleted should be shown. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if Resources that are deleted should be shown. Defaults to false.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A comma-separated list of resource IDs to exclude or include. IDs with a - in front mean to exclude. Ex: 123,-456 means to include Resource 123, but always exclude Resource 456.',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values should be rendered as they would on the resource being summarized. Defaults to false.',
              'area_trans' => '',
            ),
            'processTVList' => 
            array (
              'name' => 'processTVList',
              'desc' => 'Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits processed TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for processing if specified. Defaults to empty.',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Indicates if TemplateVar values that are not processed fully should be prepared before being returned. Defaults to true.',
              'area_trans' => '',
            ),
            'prepareTVList' => 
            array (
              'name' => 'prepareTVList',
              'desc' => 'Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Limits prepared TVs to those specified as a comma-delimited list of TV names; note only includedTVs will be available for preparing if specified. Defaults to empty.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'The prefix for TemplateVar properties. Defaults to: tv.',
              'type' => 'textfield',
              'options' => '',
              'value' => 'tv.',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The prefix for TemplateVar properties. Defaults to: tv.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'You can define the starting idx of the resources, which is an property that is incremented as each resource is rendered.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'Define the idx which represents the first resource (see tplFirst). Defaults to 1.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Define the idx which represents the first resource (see tplFirst). Defaults to 1.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Define the idx which represents the last resource (see tplLast). Defaults to the number of resources being summarized + first - 1',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'If set, will assign the result to this placeholder instead of outputting it directly.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If set, will assign the result to this placeholder instead of outputting it directly.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If set, will assign EACH result to a separate placeholder named by this param suffixed with a sequential number (starting from 0).',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'If true, will send the SQL query to the MODX log. Defaults to false.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If true, will send the SQL query to the MODX log. Defaults to false.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A JSON expression of criteria to build any additional where clauses from, e.g. &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`',
              'area_trans' => '',
            ),
            'dbCacheFlag' => 
            array (
              'name' => 'dbCacheFlag',
              'desc' => 'Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set',
              'type' => 'textfield',
              'options' => '',
              'value' => '0',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Determines how result sets are cached if cache_db is enabled in MODX. 0|false = do not cache result set; 1 = cache result set according to cache settings, any other integer value = number of seconds to cache result set',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A comma-delimited list of context keys for limiting results. Default is empty, i.e. do not limit results by context.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A condition to compare against the conditionalTpls property to map Resources to different tpls based on custom conditional logic.',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'An optional operator to use for the tplCondition when comparing against the conditionalTpls operands. Default is == (equals).',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'A JSON map of conditional operands and tpls to compare against the tplCondition property using the specified tplOperator.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getResources
 *
 * A general purpose Resource listing and summarization snippet for MODX 2.x.
 *
 * @author Jason Coward
 * @copyright Copyright 2010-2013, Jason Coward
 *
 * TEMPLATES
 *
 * tpl - Name of a chunk serving as a resource template
 * [NOTE: if not provided, properties are dumped to output for each resource]
 *
 * tplOdd - (Opt) Name of a chunk serving as resource template for resources with an odd idx value
 * (see idx property)
 * tplFirst - (Opt) Name of a chunk serving as resource template for the first resource (see first
 * property)
 * tplLast - (Opt) Name of a chunk serving as resource template for the last resource (see last
 * property)
 * tpl_{n} - (Opt) Name of a chunk serving as resource template for the nth resource
 *
 * tplCondition - (Opt) Defines a field of the resource to evaluate against keys defined in the
 * conditionalTpls property. Must be a resource field; does not work with Template Variables.
 * conditionalTpls - (Opt) A JSON object defining a map of field values and the associated tpl to
 * use when the field defined by tplCondition matches the value. [NOTE: tplOdd, tplFirst, tplLast,
 * and tpl_{n} will take precedence over any defined conditionalTpls]
 *
 * tplWrapper - (Opt) Name of a chunk serving as a wrapper template for the output
 * [NOTE: Does not work with toSeparatePlaceholders]
 *
 * SELECTION
 *
 * parents - Comma-delimited list of ids serving as parents
 *
 * context - (Opt) Comma-delimited list of context keys to limit results by; if empty, contexts for all specified
 * parents will be used (all contexts if 0 is specified) [default=]
 *
 * depth - (Opt) Integer value indicating depth to search for resources from each parent [default=10]
 *
 * tvFilters - (Opt) Delimited-list of TemplateVar values to filter resources by. Supports two
 * delimiters and two value search formats. The first delimiter || represents a logical OR and the
 * primary grouping mechanism.  Within each group you can provide a comma-delimited list of values.
 * These values can be either tied to a specific TemplateVar by name, e.g. myTV==value, or just the
 * value, indicating you are searching for the value in any TemplateVar tied to the Resource. An
 * example would be &tvFilters=`filter2==one,filter1==bar%||filter1==foo`
 * [NOTE: filtering by values uses a LIKE query and % is considered a wildcard.]
 * [NOTE: this only looks at the raw value set for specific Resource, i. e. there must be a value
 * specifically set for the Resource and it is not evaluated.]
 *
 * tvFiltersAndDelimiter - (Opt) Custom delimiter for logical AND, default \',\', in case you want to
 * match a literal comma in the tvFilters. E.g. &tvFiltersAndDelimiter=`&&`
 * &tvFilters=`filter1==foo,bar&&filter2==baz` [default=,]
 *
 * tvFiltersOrDelimiter - (Opt) Custom delimiter for logical OR, default \'||\', in case you want to
 * match a literal \'||\' in the tvFilters. E.g. &tvFiltersOrDelimiter=`|OR|`
 * &tvFilters=`filter1==foo||bar|OR|filter2==baz` [default=||]
 *
 * where - (Opt) A JSON expression of criteria to build any additional where clauses from. An example would be
 * &where=`{{"alias:LIKE":"foo%", "OR:alias:LIKE":"%bar"},{"OR:pagetitle:=":"foobar", "AND:description:=":"raboof"}}`
 *
 * sortby - (Opt) Field to sort by or a JSON array, e.g. {"publishedon":"ASC","createdon":"DESC"} [default=publishedon]
 * sortbyTV - (opt) A Template Variable name to sort by (if supplied, this precedes the sortby value) [default=]
 * sortbyTVType - (Opt) A data type to CAST a TV Value to in order to sort on it properly [default=string]
 * sortbyAlias - (Opt) Query alias for sortby field [default=]
 * sortbyEscaped - (Opt) Escapes the field name(s) specified in sortby [default=0]
 * sortdir - (Opt) Order which to sort by [default=DESC]
 * sortdirTV - (Opt) Order which to sort by a TV [default=DESC]
 * limit - (Opt) Limits the number of resources returned [default=5]
 * offset - (Opt) An offset of resources returned by the criteria to skip [default=0]
 * dbCacheFlag - (Opt) Controls caching of db queries; 0|false = do not cache result set; 1 = cache result set
 * according to cache settings, any other integer value = number of seconds to cache result set [default=0]
 *
 * OPTIONS
 *
 * includeContent - (Opt) Indicates if the content of each resource should be returned in the
 * results [default=0]
 * includeTVs - (Opt) Indicates if TemplateVar values should be included in the properties available
 * to each resource template [default=0]
 * includeTVList - (Opt) Limits the TemplateVars that are included if includeTVs is true to those specified
 * by name in a comma-delimited list [default=]
 * prepareTVs - (Opt) Prepares media-source dependent TemplateVar values [default=1]
 * prepareTVList - (Opt) Limits the TVs that are prepared to those specified by name in a comma-delimited
 * list [default=]
 * processTVs - (Opt) Indicates if TemplateVar values should be rendered as they would on the
 * resource being summarized [default=0]
 * processTVList - (opt) Limits the TemplateVars that are processed if included to those specified
 * by name in a comma-delimited list [default=]
 * tvPrefix - (Opt) The prefix for TemplateVar properties [default=tv.]
 * idx - (Opt) You can define the starting idx of the resources, which is an property that is
 * incremented as each resource is rendered [default=1]
 * first - (Opt) Define the idx which represents the first resource (see tplFirst) [default=1]
 * last - (Opt) Define the idx which represents the last resource (see tplLast) [default=# of
 * resources being summarized + first - 1]
 * outputSeparator - (Opt) An optional string to separate each tpl instance [default="\\n"]
 * wrapIfEmpty - (Opt) Indicates if the tplWrapper should be applied if the output is empty [default=0]
 *
 */
$output = array();
$outputSeparator = isset($outputSeparator) ? $outputSeparator : "\\n";

/* set default properties */
$tpl = !empty($tpl) ? $tpl : \'\';
$includeContent = !empty($includeContent) ? true : false;
$includeTVs = !empty($includeTVs) ? true : false;
$includeTVList = !empty($includeTVList) ? explode(\',\', $includeTVList) : array();
$processTVs = !empty($processTVs) ? true : false;
$processTVList = !empty($processTVList) ? explode(\',\', $processTVList) : array();
$prepareTVs = !empty($prepareTVs) ? true : false;
$prepareTVList = !empty($prepareTVList) ? explode(\',\', $prepareTVList) : array();
$tvPrefix = isset($tvPrefix) ? $tvPrefix : \'tv.\';
$parents = (!empty($parents) || $parents === \'0\') ? explode(\',\', $parents) : array($modx->resource->get(\'id\'));
array_walk($parents, \'trim\');
$parents = array_unique($parents);
$depth = isset($depth) ? (integer) $depth : 10;

$tvFiltersOrDelimiter = isset($tvFiltersOrDelimiter) ? $tvFiltersOrDelimiter : \'||\';
$tvFiltersAndDelimiter = isset($tvFiltersAndDelimiter) ? $tvFiltersAndDelimiter : \',\';
$tvFilters = !empty($tvFilters) ? explode($tvFiltersOrDelimiter, $tvFilters) : array();

$where = !empty($where) ? $modx->fromJSON($where) : array();
$showUnpublished = !empty($showUnpublished) ? true : false;
$showDeleted = !empty($showDeleted) ? true : false;

$sortby = isset($sortby) ? $sortby : \'publishedon\';
$sortbyTV = isset($sortbyTV) ? $sortbyTV : \'\';
$sortbyAlias = isset($sortbyAlias) ? $sortbyAlias : \'modResource\';
$sortbyEscaped = !empty($sortbyEscaped) ? true : false;
$sortdir = isset($sortdir) ? $sortdir : \'DESC\';
$sortdirTV = isset($sortdirTV) ? $sortdirTV : \'DESC\';
$limit = isset($limit) ? (integer) $limit : 5;
$offset = isset($offset) ? (integer) $offset : 0;
$totalVar = !empty($totalVar) ? $totalVar : \'total\';

$dbCacheFlag = !isset($dbCacheFlag) ? false : $dbCacheFlag;
if (is_string($dbCacheFlag) || is_numeric($dbCacheFlag)) {
    if ($dbCacheFlag == \'0\') {
        $dbCacheFlag = false;
    } elseif ($dbCacheFlag == \'1\') {
        $dbCacheFlag = true;
    } else {
        $dbCacheFlag = (integer) $dbCacheFlag;
    }
}

/* multiple context support */
$contextArray = array();
$contextSpecified = false;
if (!empty($context)) {
    $contextArray = explode(\',\',$context);
    array_walk($contextArray, \'trim\');
    $contexts = array();
    foreach ($contextArray as $ctx) {
        $contexts[] = $modx->quote($ctx);
    }
    $context = implode(\',\',$contexts);
    $contextSpecified = true;
    unset($contexts,$ctx);
} else {
    $context = $modx->quote($modx->context->get(\'key\'));
}

$pcMap = array();
$pcQuery = $modx->newQuery(\'modResource\', array(\'id:IN\' => $parents), $dbCacheFlag);
$pcQuery->select(array(\'id\', \'context_key\'));
if ($pcQuery->prepare() && $pcQuery->stmt->execute()) {
    foreach ($pcQuery->stmt->fetchAll(PDO::FETCH_ASSOC) as $pcRow) {
        $pcMap[(integer) $pcRow[\'id\']] = $pcRow[\'context_key\'];
    }
}

$children = array();
$parentArray = array();
foreach ($parents as $parent) {
    $parent = (integer) $parent;
    if ($parent === 0) {
        $pchildren = array();
        if ($contextSpecified) {
            foreach ($contextArray as $pCtx) {
                if (!in_array($pCtx, $contextArray)) {
                    continue;
                }
                $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                $pcchildren = $modx->getChildIds($parent, $depth, $options);
                if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
            }
        } else {
            $cQuery = $modx->newQuery(\'modContext\', array(\'key:!=\' => \'mgr\'));
            $cQuery->select(array(\'key\'));
            if ($cQuery->prepare() && $cQuery->stmt->execute()) {
                foreach ($cQuery->stmt->fetchAll(PDO::FETCH_COLUMN) as $pCtx) {
                    $options = $pCtx !== $modx->context->get(\'key\') ? array(\'context\' => $pCtx) : array();
                    $pcchildren = $modx->getChildIds($parent, $depth, $options);
                    if (!empty($pcchildren)) $pchildren = array_merge($pchildren, $pcchildren);
                }
            }
        }
        $parentArray[] = $parent;
    } else {
        $pContext = array_key_exists($parent, $pcMap) ? $pcMap[$parent] : false;
        if ($debug) $modx->log(modX::LOG_LEVEL_ERROR, "context for {$parent} is {$pContext}");
        if ($pContext && $contextSpecified && !in_array($pContext, $contextArray, true)) {
            $parent = next($parents);
            continue;
        }
        $parentArray[] = $parent;
        $options = !empty($pContext) && $pContext !== $modx->context->get(\'key\') ? array(\'context\' => $pContext) : array();
        $pchildren = $modx->getChildIds($parent, $depth, $options);
    }
    if (!empty($pchildren)) $children = array_merge($children, $pchildren);
    $parent = next($parents);
}
$parents = array_merge($parentArray, $children);

/* build query */
$criteria = array("modResource.parent IN (" . implode(\',\', $parents) . ")");
if ($contextSpecified) {
    $contextResourceTbl = $modx->getTableName(\'modContextResource\');
    $criteria[] = "(modResource.context_key IN ({$context}) OR EXISTS(SELECT 1 FROM {$contextResourceTbl} ctx WHERE ctx.resource = modResource.id AND ctx.context_key IN ({$context})))";
}
if (empty($showDeleted)) {
    $criteria[\'deleted\'] = \'0\';
}
if (empty($showUnpublished)) {
    $criteria[\'published\'] = \'1\';
}
if (empty($showHidden)) {
    $criteria[\'hidemenu\'] = \'0\';
}
if (!empty($hideContainers)) {
    $criteria[\'isfolder\'] = \'0\';
}
$criteria = $modx->newQuery(\'modResource\', $criteria);
if (!empty($tvFilters)) {
    $tmplVarTbl = $modx->getTableName(\'modTemplateVar\');
    $tmplVarResourceTbl = $modx->getTableName(\'modTemplateVarResource\');
    $conditions = array();
    $operators = array(
        \'<=>\' => \'<=>\',
        \'===\' => \'=\',
        \'!==\' => \'!=\',
        \'<>\' => \'<>\',
        \'==\' => \'LIKE\',
        \'!=\' => \'NOT LIKE\',
        \'<<\' => \'<\',
        \'<=\' => \'<=\',
        \'=<\' => \'=<\',
        \'>>\' => \'>\',
        \'>=\' => \'>=\',
        \'=>\' => \'=>\'
    );
    foreach ($tvFilters as $fGroup => $tvFilter) {
        $filterGroup = array();
        $filters = explode($tvFiltersAndDelimiter, $tvFilter);
        $multiple = count($filters) > 0;
        foreach ($filters as $filter) {
            $operator = \'==\';
            $sqlOperator = \'LIKE\';
            foreach ($operators as $op => $opSymbol) {
                if (strpos($filter, $op, 1) !== false) {
                    $operator = $op;
                    $sqlOperator = $opSymbol;
                    break;
                }
            }
            $tvValueField = \'tvr.value\';
            $tvDefaultField = \'tv.default_text\';
            $f = explode($operator, $filter);
            if (count($f) >= 2) {
                if (count($f) > 2) {
                    $k = array_shift($f);
                    $b = join($operator, $f);
                    $f = array($k, $b);
                }
                $tvName = $modx->quote($f[0]);
                if (is_numeric($f[1]) && !in_array($sqlOperator, array(\'LIKE\', \'NOT LIKE\'))) {
                    $tvValue = $f[1];
                    if ($f[1] == (integer)$f[1]) {
                        $tvValueField = "CAST({$tvValueField} AS SIGNED INTEGER)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS SIGNED INTEGER)";
                    } else {
                        $tvValueField = "CAST({$tvValueField} AS DECIMAL)";
                        $tvDefaultField = "CAST({$tvDefaultField} AS DECIMAL)";
                    }
                } else {
                    $tvValue = $modx->quote($f[1]);
                }
                if ($multiple) {
                    $filterGroup[] =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                } else {
                    $filterGroup =
                        "(EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.name = {$tvName} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id) " .
                        "OR EXISTS (SELECT 1 FROM {$tmplVarTbl} tv WHERE tv.name = {$tvName} AND {$tvDefaultField} {$sqlOperator} {$tvValue} AND tv.id NOT IN (SELECT tmplvarid FROM {$tmplVarResourceTbl} WHERE contentid = modResource.id)) " .
                        ")";
                }
            } elseif (count($f) == 1) {
                $tvValue = $modx->quote($f[0]);
                if ($multiple) {
                    $filterGroup[] = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                } else {
                    $filterGroup = "EXISTS (SELECT 1 FROM {$tmplVarResourceTbl} tvr JOIN {$tmplVarTbl} tv ON {$tvValueField} {$sqlOperator} {$tvValue} AND tv.id = tvr.tmplvarid WHERE tvr.contentid = modResource.id)";
                }
            }
        }
        $conditions[] = $filterGroup;
    }
    if (!empty($conditions)) {
        $firstGroup = true;
        foreach ($conditions as $cGroup => $c) {
            if (is_array($c)) {
                $first = true;
                foreach ($c as $cond) {
                    if ($first && !$firstGroup) {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_OR, null, $cGroup);
                    } else {
                        $criteria->condition($criteria->query[\'where\'][0][1], $cond, xPDOQuery::SQL_AND, null, $cGroup);
                    }
                    $first = false;
                }
            } else {
                $criteria->condition($criteria->query[\'where\'][0][1], $c, $firstGroup ? xPDOQuery::SQL_AND : xPDOQuery::SQL_OR, null, $cGroup);
            }
            $firstGroup = false;
        }
    }
}
/* include/exclude resources, via &resources=`123,-456` prop */
if (!empty($resources)) {
    $resourceConditions = array();
    $resources = explode(\',\',$resources);
    $include = array();
    $exclude = array();
    foreach ($resources as $resource) {
        $resource = (int)$resource;
        if ($resource == 0) continue;
        if ($resource < 0) {
            $exclude[] = abs($resource);
        } else {
            $include[] = $resource;
        }
    }
    if (!empty($include)) {
        $criteria->where(array(\'OR:modResource.id:IN\' => $include), xPDOQuery::SQL_OR);
    }
    if (!empty($exclude)) {
        $criteria->where(array(\'modResource.id:NOT IN\' => $exclude), xPDOQuery::SQL_AND, null, 1);
    }
}
if (!empty($where)) {
    $criteria->where($where);
}

$total = $modx->getCount(\'modResource\', $criteria);
$modx->setPlaceholder($totalVar, $total);

$fields = array_keys($modx->getFields(\'modResource\'));
if (empty($includeContent)) {
    $fields = array_diff($fields, array(\'content\'));
}
$columns = $includeContent ? $modx->getSelectColumns(\'modResource\', \'modResource\') : $modx->getSelectColumns(\'modResource\', \'modResource\', \'\', array(\'content\'), true);
$criteria->select($columns);
if (!empty($sortbyTV)) {
    $criteria->leftJoin(\'modTemplateVar\', \'tvDefault\', array(
        "tvDefault.name" => $sortbyTV
    ));
    $criteria->leftJoin(\'modTemplateVarResource\', \'tvSort\', array(
        "tvSort.contentid = modResource.id",
        "tvSort.tmplvarid = tvDefault.id"
    ));
    if (empty($sortbyTVType)) $sortbyTVType = \'string\';
    if ($modx->getOption(\'dbtype\') === \'mysql\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS SIGNED INTEGER) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(IFNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("IFNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    } elseif ($modx->getOption(\'dbtype\') === \'sqlsrv\') {
        switch ($sortbyTVType) {
            case \'integer\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS BIGINT) AS sortTV");
                break;
            case \'decimal\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DECIMAL) AS sortTV");
                break;
            case \'datetime\':
                $criteria->select("CAST(ISNULL(tvSort.value, tvDefault.default_text) AS DATETIME) AS sortTV");
                break;
            case \'string\':
            default:
                $criteria->select("ISNULL(tvSort.value, tvDefault.default_text) AS sortTV");
                break;
        }
    }
    $criteria->sortby("sortTV", $sortdirTV);
}
if (!empty($sortby)) {
    if (strpos($sortby, \'{\') === 0) {
        $sorts = $modx->fromJSON($sortby);
    } else {
        $sorts = array($sortby => $sortdir);
    }
    if (is_array($sorts)) {
        while (list($sort, $dir) = each($sorts)) {
            if ($sortbyEscaped) $sort = $modx->escape($sort);
            if (!empty($sortbyAlias)) $sort = $modx->escape($sortbyAlias) . ".{$sort}";
            $criteria->sortby($sort, $dir);
        }
    }
}
if (!empty($limit)) $criteria->limit($limit, $offset);

if (!empty($debug)) {
    $criteria->prepare();
    $modx->log(modX::LOG_LEVEL_ERROR, $criteria->toSQL());
}
$collection = $modx->getCollection(\'modResource\', $criteria, $dbCacheFlag);

$idx = !empty($idx) || $idx === \'0\' ? (integer) $idx : 1;
$first = empty($first) && $first !== \'0\' ? 1 : (integer) $first;
$last = empty($last) ? (count($collection) + $idx - 1) : (integer) $last;

/* include parseTpl */
include_once $modx->getOption(\'getresources.core_path\',null,$modx->getOption(\'core_path\').\'components/getresources/\').\'include.parsetpl.php\';

$templateVars = array();
if (!empty($includeTVs) && !empty($includeTVList)) {
    $templateVars = $modx->getCollection(\'modTemplateVar\', array(\'name:IN\' => $includeTVList));
}
/** @var modResource $resource */
foreach ($collection as $resourceId => $resource) {
    $tvs = array();
    if (!empty($includeTVs)) {
        if (empty($includeTVList)) {
            $templateVars = $resource->getMany(\'TemplateVars\');
        }
        /** @var modTemplateVar $templateVar */
        foreach ($templateVars as $tvId => $templateVar) {
            if (!empty($includeTVList) && !in_array($templateVar->get(\'name\'), $includeTVList)) continue;
            if ($processTVs && (empty($processTVList) || in_array($templateVar->get(\'name\'), $processTVList))) {
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $templateVar->renderOutput($resource->get(\'id\'));
            } else {
                $value = $templateVar->getValue($resource->get(\'id\'));
                if ($prepareTVs && method_exists($templateVar, \'prepareOutput\') && (empty($prepareTVList) || in_array($templateVar->get(\'name\'), $prepareTVList))) {
                    $value = $templateVar->prepareOutput($value);
                }
                $tvs[$tvPrefix . $templateVar->get(\'name\')] = $value;
            }
        }
    }
    $odd = ($idx & 1);
    $properties = array_merge(
        $scriptProperties
        ,array(
            \'idx\' => $idx
            ,\'first\' => $first
            ,\'last\' => $last
            ,\'odd\' => $odd
        )
        ,$includeContent ? $resource->toArray() : $resource->get($fields)
        ,$tvs
    );
    $resourceTpl = false;
    if ($idx == $first && !empty($tplFirst)) {
        $resourceTpl = parseTpl($tplFirst, $properties);
    }
    if ($idx == $last && empty($resourceTpl) && !empty($tplLast)) {
        $resourceTpl = parseTpl($tplLast, $properties);
    }
    $tplidx = \'tpl_\' . $idx;
    if (empty($resourceTpl) && !empty($$tplidx)) {
        $resourceTpl = parseTpl($$tplidx, $properties);
    }
    if ($idx > 1 && empty($resourceTpl)) {
        $divisors = getDivisors($idx);
        if (!empty($divisors)) {
            foreach ($divisors as $divisor) {
                $tplnth = \'tpl_n\' . $divisor;
                if (!empty($$tplnth)) {
                    $resourceTpl = parseTpl($$tplnth, $properties);
                    if (!empty($resourceTpl)) {
                        break;
                    }
                }
            }
        }
    }
    if ($odd && empty($resourceTpl) && !empty($tplOdd)) {
        $resourceTpl = parseTpl($tplOdd, $properties);
    }
    if (!empty($tplCondition) && !empty($conditionalTpls) && empty($resourceTpl)) {
        $conTpls = $modx->fromJSON($conditionalTpls);
        $subject = $properties[$tplCondition];
        $tplOperator = !empty($tplOperator) ? $tplOperator : \'=\';
        $tplOperator = strtolower($tplOperator);
        $tplCon = \'\';
        foreach ($conTpls as $operand => $conditionalTpl) {
            switch ($tplOperator) {
                case \'!=\':
                case \'neq\':
                case \'not\':
                case \'isnot\':
                case \'isnt\':
                case \'unequal\':
                case \'notequal\':
                    $tplCon = (($subject != $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<\':
                case \'lt\':
                case \'less\':
                case \'lessthan\':
                    $tplCon = (($subject < $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>\':
                case \'gt\':
                case \'greater\':
                case \'greaterthan\':
                    $tplCon = (($subject > $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'<=\':
                case \'lte\':
                case \'lessthanequals\':
                case \'lessthanorequalto\':
                    $tplCon = (($subject <= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'>=\':
                case \'gte\':
                case \'greaterthanequals\':
                case \'greaterthanequalto\':
                    $tplCon = (($subject >= $operand) ? $conditionalTpl : $tplCon);
                    break;
                case \'isempty\':
                case \'empty\':
                    $tplCon = empty($subject) ? $conditionalTpl : $tplCon;
                    break;
                case \'!empty\':
                case \'notempty\':
                case \'isnotempty\':
                    $tplCon = !empty($subject) && $subject != \'\' ? $conditionalTpl : $tplCon;
                    break;
                case \'isnull\':
                case \'null\':
                    $tplCon = $subject == null || strtolower($subject) == \'null\' ? $conditionalTpl : $tplCon;
                    break;
                case \'inarray\':
                case \'in_array\':
                case \'ia\':
                    $operand = explode(\',\', $operand);
                    $tplCon = in_array($subject, $operand) ? $conditionalTpl : $tplCon;
                    break;
                case \'between\':
                case \'range\':
                case \'>=<\':
                case \'><\':
                    $operand = explode(\',\', $operand);
                    $tplCon = ($subject >= min($operand) && $subject <= max($operand)) ? $conditionalTpl : $tplCon;
                    break;
                case \'==\':
                case \'=\':
                case \'eq\':
                case \'is\':
                case \'equal\':
                case \'equals\':
                case \'equalto\':
                default:
                    $tplCon = (($subject == $operand) ? $conditionalTpl : $tplCon);
                    break;
            }
        }
        if (!empty($tplCon)) {
            $resourceTpl = parseTpl($tplCon, $properties);
        }
    }
    if (!empty($tpl) && empty($resourceTpl)) {
        $resourceTpl = parseTpl($tpl, $properties);
    }
    if ($resourceTpl === false && !empty($debug)) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $output[]= $chunk->process(array(), \'<pre>\' . print_r($properties, true) .\'</pre>\');
    } else {
        $output[]= $resourceTpl;
    }
    $idx++;
}

/* output */
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
if (!empty($toSeparatePlaceholders)) {
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}

$output = implode($outputSeparator, $output);

$tplWrapper = $modx->getOption(\'tplWrapper\', $scriptProperties, false);
$wrapIfEmpty = $modx->getOption(\'wrapIfEmpty\', $scriptProperties, false);
if (!empty($tplWrapper) && ($wrapIfEmpty || !empty($output))) {
    $output = parseTpl($tplWrapper, array_merge($scriptProperties, array(\'output\' => $output)));
}

$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
    return \'\';
}
return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'FormIt' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'FormIt',
          'description' => 'A dynamic form processing snippet.',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
          'locked' => false,
          'properties' => 
          array (
            'hooks' => 
            array (
              'name' => 'hooks',
              'desc' => 'prop_formit.hooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список хуков которые будут выполнятся по очереди после того как форма пройдёт проверку. Если какой-то из хуков вернёт false, то следующии хуки не будут выполнены. Хук также может быть именем сниппета, этот сниппет будет выполнен как хук.',
              'area_trans' => '',
            ),
            'preHooks' => 
            array (
              'name' => 'preHooks',
              'desc' => 'prop_formit.prehooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список хуков которые будут выполнятся по очереди после того как форма будет загружена. Если какой-то из хуков вернёт false, то следующие хуки не будут выполнены. Например: можно предварительно устанавливать значения полей формы с помощью $scriptProperties[`hook`]->fields[`fieldname`]. Хук также может быть именем сниппета, этот сниппет будет выполнен как хук.',
              'area_trans' => '',
            ),
            'submitVar' => 
            array (
              'name' => 'submitVar',
              'desc' => 'prop_formit.submitvar_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено значение, то обработка формы не начнётся пока  POST параметр с этим именем не будет передан.',
              'area_trans' => '',
            ),
            'validate' => 
            array (
              'name' => 'validate',
              'desc' => 'prop_formit.validate_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список полей для проверки, для каждого поля пишется имя:валидатор (т.е.: username:required,email:required). Валидаторы могут быть объединены через двоеточие, например email:email:required. Этот параметр может быть задан на нескольких строках.',
              'area_trans' => '',
            ),
            'errTpl' => 
            array (
              'name' => 'errTpl',
              'desc' => 'prop_formit.errtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<span class="error">[[+error]]</span>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Шаблон сообщения об ошибке.',
              'area_trans' => '',
            ),
            'validationErrorMessage' => 
            array (
              'name' => 'validationErrorMessage',
              'desc' => 'prop_formit.validationerrormessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<p class="error">A form validation error occurred. Please check the values you have entered.</p>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A general error message to set to a placeholder if validation fails. Can contain [[+errors]] if you want to display a list of all errors at the top.',
              'area_trans' => '',
            ),
            'validationErrorBulkTpl' => 
            array (
              'name' => 'validationErrorBulkTpl',
              'desc' => 'prop_formit.validationerrorbulktpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<li>[[+error]]</li>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'HTML tpl that is used for each individual error in the generic validation error message value.',
              'area_trans' => '',
            ),
            'trimValuesBeforeValidation' => 
            array (
              'name' => 'trimValuesBeforeValidation',
              'desc' => 'prop_formit.trimvaluesdeforevalidation_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Whether or not to trim spaces from the beginning and end of values before attempting validation. Defaults to true.',
              'area_trans' => '',
            ),
            'customValidators' => 
            array (
              'name' => 'customValidators',
              'desc' => 'prop_formit.customvalidators_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список имён пользовательских валидаторов(сниппетов), которые вы планируете использовать в этой форме. Пользовательские валидаторы должны быть обязательно указаны в этом параметре, иначе они не будут работать.',
              'area_trans' => '',
            ),
            'clearFieldsOnSuccess' => 
            array (
              'name' => 'clearFieldsOnSuccess',
              'desc' => 'prop_formit.clearfieldsonsuccess_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено значение true, то поля формы будут очищатся после успешной отправки формы.',
              'area_trans' => '',
            ),
            'successMessage' => 
            array (
              'name' => 'successMessage',
              'desc' => 'prop_formit.successmessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Значение подстановщика для сообщения об успехе. Имя подстановщика устанавливается в параметре &successMessagePlaceholder, по умолчанию «fi.successMessage».',
              'area_trans' => '',
            ),
            'successMessagePlaceholder' => 
            array (
              'name' => 'successMessagePlaceholder',
              'desc' => 'prop_formit.successmessageplaceholder_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.successMessage',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Подстановщик для сообщения об успехе.',
              'area_trans' => '',
            ),
            'store' => 
            array (
              'name' => 'store',
              'desc' => 'prop_formit.store_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено true,  данные переданные через форму будет сохранятcя в кэше, для дальнейшего их использования с помощью сниппета FormItRetriever.',
              'area_trans' => '',
            ),
            'placeholderPrefix' => 
            array (
              'name' => 'placeholderPrefix',
              'desc' => 'prop_formit.placeholderprefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Префикс который используется всеми подстановщиками установлеными FormIt для полей. По умолчанию «fi.»',
              'area_trans' => '',
            ),
            'storeTime' => 
            array (
              'name' => 'storeTime',
              'desc' => 'prop_formit.storetime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '300',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если выбрано `запоминание` данных формы, то этот параметр указывает время(в секундах)  для хранения данных из отправленной формы. По умолчанию пять минут.',
              'area_trans' => '',
            ),
            'storeLocation' => 
            array (
              'name' => 'storeLocation',
              'desc' => 'prop_formit.storelocation_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'cache',
                  'text' => 'formit.opt_cache',
                  'name' => 'MODX Cache',
                ),
                1 => 
                array (
                  'value' => 'session',
                  'text' => 'formit.opt_session',
                  'name' => 'Session',
                ),
              ),
              'value' => 'cache',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `store` is set to true, this specifies the cache location of the data from the form submission. Defaults to MODX cache.',
              'area_trans' => '',
            ),
            'allowFiles' => 
            array (
              'name' => 'allowFiles',
              'desc' => 'prop_formit.allowfiles_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set to 0, will prevent files from being submitted on the form.',
              'area_trans' => '',
            ),
            'spamEmailFields' => 
            array (
              'name' => 'spamEmailFields',
              'desc' => 'prop_formit.spamemailfields_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «spam». Разделённый запятыми список полей содержащих адреса электронной почты для проверки на причастность к спаму.',
              'area_trans' => '',
            ),
            'spamCheckIp' => 
            array (
              'name' => 'spamCheckIp',
              'desc' => 'prop_formit.spamcheckip_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «spam». Если это параметр установлен в true, то будет проверяться ip-адресс отправителя формы на причастность к спаму.',
              'area_trans' => '',
            ),
            'recaptchaJs' => 
            array (
              'name' => 'recaptchaJs',
              'desc' => 'prop_formit.recaptchajs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '{}',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «recaptcha».  JSON объект который содержит в себе  настройки для виджета reCaptcha.',
              'area_trans' => '',
            ),
            'recaptchaTheme' => 
            array (
              'name' => 'recaptchaTheme',
              'desc' => 'prop_formit.recaptchatheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'red',
                  'text' => 'formit.opt_red',
                  'name' => 'Red',
                ),
                1 => 
                array (
                  'value' => 'white',
                  'text' => 'formit.opt_white',
                  'name' => 'White',
                ),
                2 => 
                array (
                  'value' => 'clean',
                  'text' => 'formit.opt_clean',
                  'name' => 'Clean',
                ),
                3 => 
                array (
                  'value' => 'blackglass',
                  'text' => 'formit.opt_blackglass',
                  'name' => 'Black Glass',
                ),
              ),
              'value' => 'clean',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «recaptcha». Тема оформления для виджета reCaptcha.',
              'area_trans' => '',
            ),
            'redirectTo' => 
            array (
              'name' => 'redirectTo',
              'desc' => 'prop_formit.redirectto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «redirect». В этом параметре надо указать идентификатор ресурса на который будет происходить редирект после успешной отправки формы.',
              'area_trans' => '',
            ),
            'redirectParams' => 
            array (
              'name' => 'redirectParams',
              'desc' => 'prop_formit.redirectparams_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => ' JSON array of parameters to pass to the redirect hook that will be passed when redirecting.',
              'area_trans' => '',
            ),
            'emailTo' => 
            array (
              'name' => 'emailTo',
              'desc' => 'prop_formit.emailto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Разделённый запятыми список адресов электронной почты на которые надо послать письмо.',
              'area_trans' => '',
            ),
            'emailToName' => 
            array (
              'name' => 'emailToName',
              'desc' => 'prop_formit.emailtoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailTo».',
              'area_trans' => '',
            ),
            'emailFrom' => 
            array (
              'name' => 'emailFrom',
              'desc' => 'prop_formit.emailfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Если этот параметр установлен, то он будет определять адрес электронной почты отправителя письма. Если не установлен, то сначала адрес электронной почты будет искаться в данных формы  в поле с именем «email», если это поле не будет найдено, то будет использоваться  адрес электронной почты из системной настройки «emailsender».',
              'area_trans' => '',
            ),
            'emailFromName' => 
            array (
              'name' => 'emailFromName',
              'desc' => 'prop_formit.emailfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Имя отправителя письма.',
              'area_trans' => '',
            ),
            'emailReplyTo' => 
            array (
              'name' => 'emailReplyTo',
              'desc' => 'prop_formit.emailreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Адрес электронной почты для ответа на письмо.',
              'area_trans' => '',
            ),
            'emailReplyToName' => 
            array (
              'name' => 'emailReplyToName',
              'desc' => 'prop_formit.emailreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Имя владельца адреса электронной почты который используется для ответа на письмо.',
              'area_trans' => '',
            ),
            'emailCC' => 
            array (
              'name' => 'emailCC',
              'desc' => 'prop_formit.emailcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Разделённый запятыми список адресов электронной почты на которые надо послать копию письма.',
              'area_trans' => '',
            ),
            'emailCCName' => 
            array (
              'name' => 'emailCCName',
              'desc' => 'prop_formit.emailccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailCC».',
              'area_trans' => '',
            ),
            'emailBCC' => 
            array (
              'name' => 'emailBCC',
              'desc' => 'prop_formit.emailbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email».  Разделённый запятыми список адресов  электронной почты на которые надо послать скрытую копию письма.',
              'area_trans' => '',
            ),
            'emailBCCName' => 
            array (
              'name' => 'emailBCCName',
              'desc' => 'prop_formit.emailbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailBCC».',
              'area_trans' => '',
            ),
            'emailReturnPath' => 
            array (
              'name' => 'emailReturnPath',
              'desc' => 'prop_formit.emailreturnpath_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Return-path: address for the email. If not set, will take the value of `emailFrom` property.',
              'area_trans' => '',
            ),
            'emailSubject' => 
            array (
              'name' => 'emailSubject',
              'desc' => 'prop_formit.emailsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». В этом параметре можно указать тему электронного письма.',
              'area_trans' => '',
            ),
            'emailUseFieldForSubject' => 
            array (
              'name' => 'emailUseFieldForSubject',
              'desc' => 'prop_formit.emailusefieldforsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если поле «subject» используется в форме, и это параметр установлен в true,то содержимое этого поля будет использоваться как тема электронного письма.',
              'area_trans' => '',
            ),
            'emailHtml' => 
            array (
              'name' => 'emailHtml',
              'desc' => 'prop_formit.emailhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Этот параметр включает использование html разметки в электронном письме. По умолчанию включено.',
              'area_trans' => '',
            ),
            'emailConvertNewlines' => 
            array (
              'name' => 'emailConvertNewlines',
              'desc' => 'prop_formit.emailconvertnewlines_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true and emailHtml is set to 1, will convert newlines to BR tags in the email.',
              'area_trans' => '',
            ),
            'emailMultiWrapper' => 
            array (
              'name' => 'emailMultiWrapper',
              'desc' => 'prop_formit.emailmultiwrapper_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[+value]]',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Will wrap each item in a collection of fields sent via checkboxes/multi-selects. Defaults to just the value.',
              'area_trans' => '',
            ),
            'emailMultiSeparator' => 
            array (
              'name' => 'emailMultiSeparator',
              'desc' => 'prop_formit.emailmultiseparator_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The default separator for collections of items sent through checkboxes/multi-selects. Defaults to a newline.',
              'area_trans' => '',
            ),
            'fiarTpl' => 
            array (
              'name' => 'fiarTpl',
              'desc' => 'prop_formit.fiartpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartpl_desc',
              'area_trans' => '',
            ),
            'fiarToField' => 
            array (
              'name' => 'fiarToField',
              'desc' => 'prop_formit.fiartofield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartofield_desc',
              'area_trans' => '',
            ),
            'fiarSubject' => 
            array (
              'name' => 'fiarSubject',
              'desc' => 'prop_formit.fiarsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[++site_name]] Auto-Responder',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarsubject_desc',
              'area_trans' => '',
            ),
            'fiarFrom' => 
            array (
              'name' => 'fiarFrom',
              'desc' => 'prop_formit.fiarfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfrom_desc',
              'area_trans' => '',
            ),
            'fiarFromName' => 
            array (
              'name' => 'fiarFromName',
              'desc' => 'prop_formit.fiarfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfromname_desc',
              'area_trans' => '',
            ),
            'fiarReplyTo' => 
            array (
              'name' => 'fiarReplyTo',
              'desc' => 'prop_formit.fiarreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplyto_desc',
              'area_trans' => '',
            ),
            'fiarReplyToName' => 
            array (
              'name' => 'fiarReplyToName',
              'desc' => 'prop_formit.fiarreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplytoname_desc',
              'area_trans' => '',
            ),
            'fiarCC' => 
            array (
              'name' => 'fiarCC',
              'desc' => 'prop_formit.fiarcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarcc_desc',
              'area_trans' => '',
            ),
            'fiarCCName' => 
            array (
              'name' => 'fiarCCName',
              'desc' => 'prop_fiar.fiarccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «FormItAutoResponder». Необязательный параметр.  Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailCC».',
              'area_trans' => '',
            ),
            'fiarBCC' => 
            array (
              'name' => 'fiarBCC',
              'desc' => 'prop_formit.fiarbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbcc_desc',
              'area_trans' => '',
            ),
            'fiarBCCName' => 
            array (
              'name' => 'fiarBCCName',
              'desc' => 'prop_formit.fiarbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbccname_desc',
              'area_trans' => '',
            ),
            'fiarHtml' => 
            array (
              'name' => 'fiarHtml',
              'desc' => 'prop_formit.fiarhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarhtml_desc',
              'area_trans' => '',
            ),
            'mathMinRange' => 
            array (
              'name' => 'mathMinRange',
              'desc' => 'prop_formit.mathminrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '10',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathminrange_desc',
              'area_trans' => '',
            ),
            'mathMaxRange' => 
            array (
              'name' => 'mathMaxRange',
              'desc' => 'prop_formit.mathmaxrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '100',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathmaxrange_desc',
              'area_trans' => '',
            ),
            'mathField' => 
            array (
              'name' => 'mathField',
              'desc' => 'prop_formit.mathfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'math',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathfield_desc',
              'area_trans' => '',
            ),
            'mathOp1Field' => 
            array (
              'name' => 'mathOp1Field',
              'desc' => 'prop_formit.mathop1field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op1',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop1field_desc',
              'area_trans' => '',
            ),
            'mathOp2Field' => 
            array (
              'name' => 'mathOp2Field',
              'desc' => 'prop_formit.mathop2field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op2',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop2field_desc',
              'area_trans' => '',
            ),
            'mathOperatorField' => 
            array (
              'name' => 'mathOperatorField',
              'desc' => 'prop_formit.mathoperatorfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'operator',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathoperatorfield_desc',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Заголовок страницы',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'desc' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'desc',
          'caption' => 'Описание',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'keyw' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'keyw',
          'caption' => 'Ключевые слова',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);