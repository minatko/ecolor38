<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 5,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Экраны',
    'longtitle' => 'Экраны под ванну и душевые поддоны',
    'description' => '',
    'alias' => 'ekrany-pod-vannu-i-dushevye-poddony',
    'alias_visible' => 1,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<div id="pu55361" class="clearfix grpelem">
<div id="u7953-25" class="clearfix colelem">
<p>Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт. Можно сказать, что о реставрации оборудования  для ванных комнат мы знаем все. И знаем, как сделать так, чтобы ваша ванная преобразилась, превратилась из старой и обшарпанной — в новую и современную.</p>
<p>Также у нас можно купить душевую кабину, ванну, экраны для ванн и душевые поддоны. Долговременное сотрудничество с проверенными поставщиками позволяет нам предлагать только высококачественную и очень эстетичную продукцию.</p>
<p> Экран под ванну — это конструкция с распорками и регулируемыми по высоте ножками, упирающимися в пол и в бортик ванны. Полотно экрана бывает глухими или решетчатым. Он закрывает борта ванны и одновременно организует пространство под ванной. Экран обеспечивает легкий доступ к коммуникация и позволяет комфортно хранить под ванной разные необходимые там вещи. Мы предлагаем несколько коллекций экранов под ванну.</p>
<p> Экраны  «Still» и «Plast» имеют высокое качество, доступную цену и более усовершенствованную конструкцию каркаса, чем другие модели. Каждый экран представлен большим разнообразием декорирования: от белого до тропической ночи.</p>
<p> Душевые кабины в Иркутске, как и в других городах, становятся все более популярными. То и дело их устанавливают вместо привычных ванн. Одним из важнейших аксессуаров для душевых кабин является душевой поддон.</p>
<p> Мы предлагаем душевые поддоны с большим разнообразием форм и материалов выполнения. Вы можете выбрать то, что оптимальным образом подойдет для вас. Подобрать подходящий материал и форму поддона, страну-изготовителя и другие параметры.</p>
<p> Ванная — это такое особое место, где начинается и заканчивается наш день. Утром хочется, зайдя в ванную чтобы умыться, создать себе хорошее настроение. А вечером в ванне так приятно полежать и расслабиться! Поэтому мы стараемся сделать помещение ванной комнаты не только удобным и функциональным, но также и красивым, ярким, индивидуальным для каждого. Обращайтесь — и мы обязательно подберем все необходимое для вашей ванной.</p>
<p> Цены указаны на странице услуг и цен.</p>
<h2 style="text-align: center;"><span>Цветовая гамма экранов</span></h2>
</div>
</div>
<div align="center">
<div class="hachi">
<div class="zaghachi">01 - белый глянец</div>
<div class="ekran"><img src="images/01%20-%20%d0%b1%d0%b5%d0%bb%d1%8b%d0%b9%20%d0%b3%d0%bb%d1%8f%d0%bd%d0%b5%d1%86.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>02 - коричневый камень</span></div>
<div class="ekran"><img src="images/02%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>03 - светло-розовый камень</span></div>
<div class="ekran"><img src="images/03%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>04 - плитка</span></div>
<div class="ekran"><img src="images/04%20-%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>05 - светло-коричневый дождь</span></div>
<div class="ekran"><img src="images/05%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%b4%d0%be%d0%b6%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>06 - светлый серо-зеленый мрамор</span></div>
<div class="ekran"><img src="images/06%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d1%8b%d0%b9%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>07 - серо-зеленый мрамор</span></div>
<div class="ekran"><img src="images/07%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>10 - морской бриз</span></div>
<div class="ekran"><img src="images/08%20-%20%d1%81%d0%b8%d0%bd%d0%b8%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>10 - морской бриз</span></div>
<div class="ekran"><img src="images/10%20-%20%d0%bc%d0%be%d1%80%d1%81%d0%ba%d0%be%d0%b9%20%d0%b1%d1%80%d0%b8%d0%b7.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span><span>12 - светло-зеленый мрамор</span></span></div>
<div class="ekran"><img src="images/12%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>14 - темно-зеленый камень</span></div>
<div class="ekran"><img src="images/14%20-%20%d1%82%d0%b5%d0%bc%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>18 - розовый мрамор</span></div>
<div class="ekran"><img src="images/16%20-%20%d1%87%d0%b5%d1%80%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>18 - розовый мрамор</span></div>
<div class="ekran"><img src="images/18%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>20 - серый мрамор</span></div>
<div class="ekran"><img src="images/19%20-%20%d1%81%d0%b8%d0%bd%d0%b5-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>20 - серый мрамор</span></div>
<div class="ekran"><img src="images/20%20-%20%d1%81%d0%b5%d1%80%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>21 - желтый камень</span></div>
<div class="ekran"><img src="images/21%20-%20%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>24 - розовый мрамор</span></div>
<div class="ekran"><img src="images/22%20-%20%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>26 - бежевый мрамор</span></div>
<div class="ekran"><img src="images/18%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>26 - бежевый мрамор</span></div>
<div class="ekran"><img src="images/19%20-%20%d1%81%d0%b8%d0%bd%d0%b5-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>27 - серо-бежевый мрамор</span></div>
<div class="ekran"><img src="images/27%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b1%d0%b5%d0%b6%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>28 - красно-коричневый мрамор</span></div>
<div class="ekran"><img src="images/28%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d0%be-%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>29 - светло-голубой мрамор</span></div>
<div class="ekran"><img src="images/29%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>30 - голубой мрамор</span></div>
<div class="ekran"><img src="images/30%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>31 - серый дождь</span></div>
<div class="ekran"><img src="images/31%20-%20%d1%81%d0%b5%d1%80%d1%8b%d0%b9%20%d0%b4%d0%be%d0%b6%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>34 - коричнево-розовый мрамор</span></div>
<div class="ekran"><img src="images/34%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>35 - светло-зеленый камень</span></div>
<div class="ekran"><img src="images/35%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>38 - вода</span></div>
<div class="ekran"><img src="images/38%20-%20%d0%b2%d0%be%d0%b4%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>41 - голубая смальта</span></div>
<div class="ekran"><img src="images/41%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>45 - кофе с молоком</span></div>
<div class="ekran"><img src="images/45%20-%20%d0%ba%d0%be%d1%84%d0%b5%20%d1%81%20%d0%bc%d0%be%d0%bb%d0%be%d0%ba%d0%be%d0%bc.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>47 - серо-зеленый камень</span></div>
<div class="ekran"><img src="images/47%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>50 - капли</span></div>
<div class="ekran"><img src="images/35%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>50 - капли</span></div>
<div class="ekran"><img src="images/50%20-%20%d0%ba%d0%b0%d0%bf%d0%bb%d0%b8.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>52 - светло-зеленая смальта</span></div>
<div class="ekran"><img src="images/52%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>54 - фисташковый</span></div>
<div class="ekran"><img src="images/54%20-%20%d1%84%d0%b8%d1%81%d1%82%d0%b0%d1%88%d0%ba%d0%be%d0%b2%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>55 - лунный</span></div>
<div class="ekran"><img src="images/55%20-%20%d0%bb%d1%83%d0%bd%d0%bd%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>56 - красно-зеленый мрамор</span></div>
<div class="ekran"><img src="images/56%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>57 - пучина</span></div>
<div class="ekran"><img src="images/57%20-%20%d0%bf%d1%83%d1%87%d0%b8%d0%bd%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>59 - розовый иней</span></div>
<div class="ekran"><img src="images/59%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%b8%d0%bd%d0%b5%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>60 - голубой иней</span></div>
<div class="ekran"><img src="images/60%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%b8%d0%bd%d0%b5%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>61 - розово-желтый камень</span></div>
<div class="ekran"><img src="images/61%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d0%be-%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>62 - бежевая смальта</span></div>
<div class="ekran"><img src="images/62%20-%20%d0%b1%d0%b5%d0%b6%d0%b5%d0%b2%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>63 - древесный</span></div>
<div class="ekran"><img src="images/63%20-%20%d0%b4%d1%80%d0%b5%d0%b2%d0%b5%d1%81%d0%bd%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>64  - сахара</span></div>
<div class="ekran"><img src="images/64%20%d1%81%d0%b0%d1%85%d0%b0%d1%80%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>65 - сиреневый мрамор</span></div>
<div class="ekran"><img src="images/65%20-%20%d1%81%d0%b8%d1%80%d0%b5%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>69 - красный камень</span></div>
<div class="ekran"><img src="images/69%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>71 - светло-салатовый камень</span></div>
<div class="ekran"><img src="images/71%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d1%81%d0%b0%d0%bb%d0%b0%d1%82%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>73 - светло-желтый камень</span></div>
<div class="ekran"><img src="images/73%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>74 - бледно-зеленый мрамор</span></div>
<div class="ekran"><img src="images/74%20-%20%d0%b1%d0%bb%d0%b5%d0%b4%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>75 - коричнево-розовый камень</span></div>
<div class="ekran"><img src="images/75%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>76 - песчаный камень</span></div>
<div class="ekran"><img src="images/76%20-%20%d0%bf%d0%b5%d1%81%d1%87%d0%b0%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>77 - Зеленая плитка</span></div>
<div class="ekran"><img src="images/77%20-%20%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d0%b0%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span><span>78 - Песочная мозаика</span><br /></span></div>
<div class="ekran"><img src="images/78%20-%20%d0%bf%d0%b5%d1%81%d0%be%d1%87%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/79%20-%20%d0%b2%d0%be%d0%b4%d0%bd%d0%b0%d1%8f%20%d0%b3%d0%bb%d0%b0%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/80%20-%20%d1%81%d0%b5%d1%80%d0%b0%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/81%20-%20%d1%81%d0%b8%d0%bd%d1%8f%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>82 - Мозаика</span></div>
<div class="ekran"><img src="images/82%20-%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>83 - Орех</span></div>
<div class="ekran"><img src="images/83%20-%20%d0%be%d1%80%d0%b5%d1%85.jpg" alt="" width="168" height="101" /></div>
</div>
</div>',
    'richtext' => 1,
    'template' => 5,
    'menuindex' => 4,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1484725498,
    'editedby' => 1,
    'editedon' => 1485336210,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1484725440,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'ekrany-pod-vannu-i-dushevye-poddony.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'desc' => 
    array (
      0 => 'desc',
      1 => 'Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт.',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'titl' => 
    array (
      0 => 'titl',
      1 => 'Экраны под ванну и душевые поддоны',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="https://ecolor38.ru/"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>Экраны под ванну и душевые поддоны</title>
  <meta name="description" content="Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт." />
<meta name="keywords" content="реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">
    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>
     <div id="top_nav" class="menu">
    <ul>
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul> </div>  

     <h1>Экраны под ванну и душевые поддоны</h1>
<div class="content">
    <div id="pu55361" class="clearfix grpelem">
<div id="u7953-25" class="clearfix colelem">
<p>Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт. Можно сказать, что о реставрации оборудования  для ванных комнат мы знаем все. И знаем, как сделать так, чтобы ваша ванная преобразилась, превратилась из старой и обшарпанной — в новую и современную.</p>
<p>Также у нас можно купить душевую кабину, ванну, экраны для ванн и душевые поддоны. Долговременное сотрудничество с проверенными поставщиками позволяет нам предлагать только высококачественную и очень эстетичную продукцию.</p>
<p> Экран под ванну — это конструкция с распорками и регулируемыми по высоте ножками, упирающимися в пол и в бортик ванны. Полотно экрана бывает глухими или решетчатым. Он закрывает борта ванны и одновременно организует пространство под ванной. Экран обеспечивает легкий доступ к коммуникация и позволяет комфортно хранить под ванной разные необходимые там вещи. Мы предлагаем несколько коллекций экранов под ванну.</p>
<p> Экраны  «Still» и «Plast» имеют высокое качество, доступную цену и более усовершенствованную конструкцию каркаса, чем другие модели. Каждый экран представлен большим разнообразием декорирования: от белого до тропической ночи.</p>
<p> Душевые кабины в Иркутске, как и в других городах, становятся все более популярными. То и дело их устанавливают вместо привычных ванн. Одним из важнейших аксессуаров для душевых кабин является душевой поддон.</p>
<p> Мы предлагаем душевые поддоны с большим разнообразием форм и материалов выполнения. Вы можете выбрать то, что оптимальным образом подойдет для вас. Подобрать подходящий материал и форму поддона, страну-изготовителя и другие параметры.</p>
<p> Ванная — это такое особое место, где начинается и заканчивается наш день. Утром хочется, зайдя в ванную чтобы умыться, создать себе хорошее настроение. А вечером в ванне так приятно полежать и расслабиться! Поэтому мы стараемся сделать помещение ванной комнаты не только удобным и функциональным, но также и красивым, ярким, индивидуальным для каждого. Обращайтесь — и мы обязательно подберем все необходимое для вашей ванной.</p>
<p> Цены указаны на странице услуг и цен.</p>
<h2 style="text-align: center;"><span>Цветовая гамма экранов</span></h2>
</div>
</div>
<div align="center">
<div class="hachi">
<div class="zaghachi">01 - белый глянец</div>
<div class="ekran"><img src="images/01%20-%20%d0%b1%d0%b5%d0%bb%d1%8b%d0%b9%20%d0%b3%d0%bb%d1%8f%d0%bd%d0%b5%d1%86.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>02 - коричневый камень</span></div>
<div class="ekran"><img src="images/02%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>03 - светло-розовый камень</span></div>
<div class="ekran"><img src="images/03%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>04 - плитка</span></div>
<div class="ekran"><img src="images/04%20-%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>05 - светло-коричневый дождь</span></div>
<div class="ekran"><img src="images/05%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%b4%d0%be%d0%b6%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>06 - светлый серо-зеленый мрамор</span></div>
<div class="ekran"><img src="images/06%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d1%8b%d0%b9%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>07 - серо-зеленый мрамор</span></div>
<div class="ekran"><img src="images/07%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>10 - морской бриз</span></div>
<div class="ekran"><img src="images/08%20-%20%d1%81%d0%b8%d0%bd%d0%b8%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>10 - морской бриз</span></div>
<div class="ekran"><img src="images/10%20-%20%d0%bc%d0%be%d1%80%d1%81%d0%ba%d0%be%d0%b9%20%d0%b1%d1%80%d0%b8%d0%b7.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span><span>12 - светло-зеленый мрамор</span></span></div>
<div class="ekran"><img src="images/12%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>14 - темно-зеленый камень</span></div>
<div class="ekran"><img src="images/14%20-%20%d1%82%d0%b5%d0%bc%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>18 - розовый мрамор</span></div>
<div class="ekran"><img src="images/16%20-%20%d1%87%d0%b5%d1%80%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>18 - розовый мрамор</span></div>
<div class="ekran"><img src="images/18%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>20 - серый мрамор</span></div>
<div class="ekran"><img src="images/19%20-%20%d1%81%d0%b8%d0%bd%d0%b5-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>20 - серый мрамор</span></div>
<div class="ekran"><img src="images/20%20-%20%d1%81%d0%b5%d1%80%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>21 - желтый камень</span></div>
<div class="ekran"><img src="images/21%20-%20%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>24 - розовый мрамор</span></div>
<div class="ekran"><img src="images/22%20-%20%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>26 - бежевый мрамор</span></div>
<div class="ekran"><img src="images/18%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>26 - бежевый мрамор</span></div>
<div class="ekran"><img src="images/19%20-%20%d1%81%d0%b8%d0%bd%d0%b5-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>27 - серо-бежевый мрамор</span></div>
<div class="ekran"><img src="images/27%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b1%d0%b5%d0%b6%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>28 - красно-коричневый мрамор</span></div>
<div class="ekran"><img src="images/28%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d0%be-%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>29 - светло-голубой мрамор</span></div>
<div class="ekran"><img src="images/29%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>30 - голубой мрамор</span></div>
<div class="ekran"><img src="images/30%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>31 - серый дождь</span></div>
<div class="ekran"><img src="images/31%20-%20%d1%81%d0%b5%d1%80%d1%8b%d0%b9%20%d0%b4%d0%be%d0%b6%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>34 - коричнево-розовый мрамор</span></div>
<div class="ekran"><img src="images/34%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>35 - светло-зеленый камень</span></div>
<div class="ekran"><img src="images/35%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>38 - вода</span></div>
<div class="ekran"><img src="images/38%20-%20%d0%b2%d0%be%d0%b4%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>41 - голубая смальта</span></div>
<div class="ekran"><img src="images/41%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>45 - кофе с молоком</span></div>
<div class="ekran"><img src="images/45%20-%20%d0%ba%d0%be%d1%84%d0%b5%20%d1%81%20%d0%bc%d0%be%d0%bb%d0%be%d0%ba%d0%be%d0%bc.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>47 - серо-зеленый камень</span></div>
<div class="ekran"><img src="images/47%20-%20%d1%81%d0%b5%d1%80%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>50 - капли</span></div>
<div class="ekran"><img src="images/35%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>50 - капли</span></div>
<div class="ekran"><img src="images/50%20-%20%d0%ba%d0%b0%d0%bf%d0%bb%d0%b8.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>52 - светло-зеленая смальта</span></div>
<div class="ekran"><img src="images/52%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>54 - фисташковый</span></div>
<div class="ekran"><img src="images/54%20-%20%d1%84%d0%b8%d1%81%d1%82%d0%b0%d1%88%d0%ba%d0%be%d0%b2%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>55 - лунный</span></div>
<div class="ekran"><img src="images/55%20-%20%d0%bb%d1%83%d0%bd%d0%bd%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>56 - красно-зеленый мрамор</span></div>
<div class="ekran"><img src="images/56%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>57 - пучина</span></div>
<div class="ekran"><img src="images/57%20-%20%d0%bf%d1%83%d1%87%d0%b8%d0%bd%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>59 - розовый иней</span></div>
<div class="ekran"><img src="images/59%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%b8%d0%bd%d0%b5%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>60 - голубой иней</span></div>
<div class="ekran"><img src="images/60%20-%20%d0%b3%d0%be%d0%bb%d1%83%d0%b1%d0%be%d0%b9%20%d0%b8%d0%bd%d0%b5%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>61 - розово-желтый камень</span></div>
<div class="ekran"><img src="images/61%20-%20%d1%80%d0%be%d0%b7%d0%be%d0%b2%d0%be-%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>62 - бежевая смальта</span></div>
<div class="ekran"><img src="images/62%20-%20%d0%b1%d0%b5%d0%b6%d0%b5%d0%b2%d0%b0%d1%8f%20%d1%81%d0%bc%d0%b0%d0%bb%d1%8c%d1%82%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>63 - древесный</span></div>
<div class="ekran"><img src="images/63%20-%20%d0%b4%d1%80%d0%b5%d0%b2%d0%b5%d1%81%d0%bd%d1%8b%d0%b9.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>64  - сахара</span></div>
<div class="ekran"><img src="images/64%20%d1%81%d0%b0%d1%85%d0%b0%d1%80%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>65 - сиреневый мрамор</span></div>
<div class="ekran"><img src="images/65%20-%20%d1%81%d0%b8%d1%80%d0%b5%d0%bd%d0%b5%d0%b2%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>69 - красный камень</span></div>
<div class="ekran"><img src="images/69%20-%20%d0%ba%d1%80%d0%b0%d1%81%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>71 - светло-салатовый камень</span></div>
<div class="ekran"><img src="images/71%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d1%81%d0%b0%d0%bb%d0%b0%d1%82%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>73 - светло-желтый камень</span></div>
<div class="ekran"><img src="images/73%20-%20%d1%81%d0%b2%d0%b5%d1%82%d0%bb%d0%be-%d0%b6%d0%b5%d0%bb%d1%82%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>74 - бледно-зеленый мрамор</span></div>
<div class="ekran"><img src="images/74%20-%20%d0%b1%d0%bb%d0%b5%d0%b4%d0%bd%d0%be-%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d1%8b%d0%b9%20%d0%bc%d1%80%d0%b0%d0%bc%d0%be%d1%80.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>75 - коричнево-розовый камень</span></div>
<div class="ekran"><img src="images/75%20-%20%d0%ba%d0%be%d1%80%d0%b8%d1%87%d0%bd%d0%b5%d0%b2%d0%be-%d1%80%d0%be%d0%b7%d0%be%d0%b2%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>76 - песчаный камень</span></div>
<div class="ekran"><img src="images/76%20-%20%d0%bf%d0%b5%d1%81%d1%87%d0%b0%d0%bd%d1%8b%d0%b9%20%d0%ba%d0%b0%d0%bc%d0%b5%d0%bd%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>77 - Зеленая плитка</span></div>
<div class="ekran"><img src="images/77%20-%20%d0%b7%d0%b5%d0%bb%d0%b5%d0%bd%d0%b0%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span><span>78 - Песочная мозаика</span><br /></span></div>
<div class="ekran"><img src="images/78%20-%20%d0%bf%d0%b5%d1%81%d0%be%d1%87%d0%bd%d0%b0%d1%8f%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/79%20-%20%d0%b2%d0%be%d0%b4%d0%bd%d0%b0%d1%8f%20%d0%b3%d0%bb%d0%b0%d0%b4%d1%8c.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/80%20-%20%d1%81%d0%b5%d1%80%d0%b0%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>80 - Серая плитка</span></div>
<div class="ekran"><img src="images/81%20-%20%d1%81%d0%b8%d0%bd%d1%8f%d1%8f%20%d0%bf%d0%bb%d0%b8%d1%82%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>82 - Мозаика</span></div>
<div class="ekran"><img src="images/82%20-%20%d0%bc%d0%be%d0%b7%d0%b0%d0%b8%d0%ba%d0%b0.jpg" alt="" width="168" height="101" /></div>
</div>
<div class="hachi">
<div class="zaghachi"><span>83 - Орех</span></div>
<div class="ekran"><img src="images/83%20-%20%d0%be%d1%80%d0%b5%d1%85.jpg" alt="" width="168" height="101" /></div>
</div>
</div>
            </div>
         <div class="answer" style="width: 100%; display: inline-block; min-height: 427.79999999999995px;padding-bottom: 0px; background: transparent url(/images/bathtub_1.jpg) no-repeat center center;
    background-size: cover;">
                  [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl`
   &emailTo=`profitvan@yandex.ru`
   &emailSubject=`Письмо с сайта https://ecolor38.ru/`
   &emailFrom=`robot@ecolor38.ru`
   &submitVar=`form1`
   &redirectTo=`31`
   &formName=`Заказать со скидкой`
   &formFields=`name,phone`
   &fieldNames=`name==Имя,phone==Телефон`
]]
                        <form action="ekrany-pod-vannu-i-dushevye-poddony.html" method="post" class="answerform" style="margin-top: 20px;padding-top: 10px;height: 340px;">
     
        <p><b>Заказать понравившийся экран<br />
Вы можете заполнив форму ниже</b><br />Наш менеджер свяжется с вами<br />
в течении 15 минут</p>
        <input type="text" name="name" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.name]]">
        <input type="text" name="phone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.phone]]">
        <input class="button" type="submit" name="form1" value="Заказать со скидкой" >
        
        <span>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</span>
        </form>
            </div>
            <div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта https://ecolor38.ru/`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="ekrany-pod-vannu-i-dushevye-poddony.html" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	
</div>
</div>   
</div>
   </body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[*titl]]' => 'Экраны под ванну и душевые поддоны',
    '[[*desc]]' => 'Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт.',
    '[[*keyw]]' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="https://ecolor38.ru/"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>Экраны под ванну и душевые поддоны</title>
  <meta name="description" content="Экраны под ванну и душевые поддоны Мы занимаемся реставрацией старых ванн, джакузи и душевых поддонов с 2010 года. За прошедшее время накопили в этом деле немалый опыт." />
<meta name="keywords" content="реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
    '[[$header]]' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
    '[[*id]]' => 5,
    '[[~3]]' => 'o-nas.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="o-nas.html">О нас</a></li>`&else=`<li><a href="o-nas.html">О нас</a></li>`]]' => '<li><a href="o-nas.html">О нас</a></li>',
    '[[~4]]' => 'nashi-uslugi.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="nashi-uslugi.html">Услуги</a></li>`&else=`<li><a href="nashi-uslugi.html">Услуги</a></li>`]]' => '<li><a href="nashi-uslugi.html">Услуги</a></li>',
    '[[~5]]' => 'ekrany-pod-vannu-i-dushevye-poddony.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>`&else=`<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>`]]' => '<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>',
    '[[~6]]' => 'tseny-na-uslugi.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="tseny-na-uslugi.html">Цены</a></li>`&else=`<li><a href="tseny-na-uslugi.html">Цены</a></li>`]]' => '<li><a href="tseny-na-uslugi.html">Цены</a></li>',
    '[[~7]]' => 'aktsiya.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="aktsiya.html">Акции</a></li>`&else=`<li><a href="aktsiya.html">Акции</a></li>`]]' => '<li><a href="aktsiya.html">Акции</a></li>',
    '[[~8]]' => 'nashi-materialy.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="nashi-materialy.html">Материалы</a></li>`&else=`<li><a href="nashi-materialy.html">Материалы</a></li>`]]' => '<li><a href="nashi-materialy.html">Материалы</a></li>',
    '[[~9]]' => 'akrilovie-vkladishi.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>`&else=`<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>`]]' => '<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>',
    '[[~10]]' => 'otzyvy-klientov.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="otzyvy-klientov.html">Отзывы</a></li>`&else=`<li><a href="otzyvy-klientov.html">Отзывы</a></li>`]]' => '<li><a href="otzyvy-klientov.html">Отзывы</a></li>',
    '[[~11]]' => '30-polezno/',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="30-polezno/">Наш блог</a></li>`&else=`<li><a href="30-polezno/">Наш блог</a></li>`]]' => '<li><a href="30-polezno/">Наш блог</a></li>',
    '[[~12]]' => 'kontakti.html',
    '[[If?   &subject=`5`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="kontakti.html">Контакты</a></li>`&else=`<li><a href="kontakti.html">Контакты</a></li>`]]' => '<li><a href="kontakti.html">Контакты</a></li>',
    '[[$menu]]' => ' <div id="top_nav" class="menu">
    <ul>
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="o-nas.html">О нас</a></li>
<li><a href="nashi-uslugi.html">Услуги</a></li>
<li><a href="ekrany-pod-vannu-i-dushevye-poddony.html">Экраны</a></li>
<li><a href="tseny-na-uslugi.html">Цены</a></li>
<li><a href="aktsiya.html">Акции</a></li>
<li><a href="nashi-materialy.html">Материалы</a></li>
<li><a href="akrilovie-vkladishi.html">Акриловые вкладыши</a></li>
<li><a href="otzyvy-klientov.html">Отзывы</a></li>
<li><a href="30-polezno/">Наш блог</a></li>
<li><a href="kontakti.html">Контакты</a></li>
    </ul> </div>  
',
    '[[*longtitle]]' => 'Экраны под ванну и душевые поддоны',
    '[[$footer]]' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта https://ecolor38.ru/`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="ekrany-pod-vannu-i-dushevye-poddony.html" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="[[++site_url]]"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>[[*titl]]</title>
  <meta name="description" content="[[*desc]]" />
<meta name="keywords" content="[[*keyw]]" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru-RU">
 <head>
  <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
  <base href="[[++site_url]]"/>
  <link rel="shortcut icon" href="images/favicon.ico"/>
  <title>[[*titl]]</title>
  <meta name="description" content="[[*desc]]" />
<meta name="keywords" content="[[*keyw]]" />
<link href="favicon.ico" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 </head>
 <body>
<div class="container">',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '    <div id="header">
        <div class="logo"><a href="/"><img src="../images/logo.png" /></a>
        </div>
        <div class="timejob">
        <div class="t1"><img style="float: left;margin-right: 10px;" src="/images/wall-clock.png"/><span style="font-size: 13px;">ПН - ВСК с 09:00 до 20:00</span><br/><span style="font-size: 13px;color:#69696d;">
Без обеда и выходных</span></div> 
        <div class="t2">
            <img style="float: left;margin: 10px 13px 0px 0px;" src="/images/location-pin.png"/>
            <p style="width:240px; font-size:12px;line-height: 14px;"><span>Работаем по Иркутску и области<br/>Доставка материала по всей России</span></p></div>
        </div>
        <div class="phone"><img style="float: left;margin: 10px 10px 0px 0px;" src="/images/call-answer.png"/>
            <p>8 (3952) 73-13-86<br/ >8 (3955) 63-67-89<br/ >8 (902) 514-67-89</p>
        </div>
    </div>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'menu' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'menu',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => ' <div id="top_nav" class="menu">
    <ul>
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~3]]">О нас</a></li>`&else=`<li><a href="[[~3]]">О нас</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~4]]">Услуги</a></li>`&else=`<li><a href="[[~4]]">Услуги</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~5]]">Экраны</a></li>`&else=`<li><a href="[[~5]]">Экраны</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~6]]">Цены</a></li>`&else=`<li><a href="[[~6]]">Цены</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~7]]">Акции</a></li>`&else=`<li><a href="[[~7]]">Акции</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~8]]">Материалы</a></li>`&else=`<li><a href="[[~8]]">Материалы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~9]]">Акриловые вкладыши</a></li>`&else=`<li><a href="[[~9]]">Акриловые вкладыши</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~10]]">Отзывы</a></li>`&else=`<li><a href="[[~10]]">Отзывы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~11]]">Наш блог</a></li>`&else=`<li><a href="[[~11]]">Наш блог</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~12]]">Контакты</a></li>`&else=`<li><a href="[[~12]]">Контакты</a></li>`]]
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="[[~3]]">О нас</a></li>
<li><a href="[[~4]]">Услуги</a></li>
<li><a href="[[~5]]">Экраны</a></li>
<li><a href="[[~6]]">Цены</a></li>
<li><a href="[[~7]]">Акции</a></li>
<li><a href="[[~8]]">Материалы</a></li>
<li><a href="[[~9]]">Акриловые вкладыши</a></li>
<li><a href="[[~10]]">Отзывы</a></li>
<li><a href="[[~11]]">Наш блог</a></li>
<li><a href="[[~12]]">Контакты</a></li>
    </ul> </div>  
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => ' <div id="top_nav" class="menu">
    <ul>
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`3` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~3]]">О нас</a></li>`&else=`<li><a href="[[~3]]">О нас</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`4` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~4]]">Услуги</a></li>`&else=`<li><a href="[[~4]]">Услуги</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`5` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~5]]">Экраны</a></li>`&else=`<li><a href="[[~5]]">Экраны</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`6` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~6]]">Цены</a></li>`&else=`<li><a href="[[~6]]">Цены</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`7` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~7]]">Акции</a></li>`&else=`<li><a href="[[~7]]">Акции</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`8` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~8]]">Материалы</a></li>`&else=`<li><a href="[[~8]]">Материалы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`9` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~9]]">Акриловые вкладыши</a></li>`&else=`<li><a href="[[~9]]">Акриловые вкладыши</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`10` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~10]]">Отзывы</a></li>`&else=`<li><a href="[[~10]]">Отзывы</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`11` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~11]]">Наш блог</a></li>`&else=`<li><a href="[[~11]]">Наш блог</a></li>`]]
[[If?   &subject=`[[*id]]`   &operator=`EQ`   &operand=`12` &then=`<li style="    background-color: #29ABE2;"><a style="color: #212434;" href="[[~12]]">Контакты</a></li>`&else=`<li><a href="[[~12]]">Контакты</a></li>`]]
    </ul>    
    </div>
    
     <div id="top_navphone" class="menuphone">
<a class="downn" href="javascript:;" onclick="$(this).parent().next().toggleClass(\'hidden\');"><b>Меню сайта</b></a>
    </div>
     <div class="hidden">
    <ul class="menuphonelist">
<li><a href="[[~3]]">О нас</a></li>
<li><a href="[[~4]]">Услуги</a></li>
<li><a href="[[~5]]">Экраны</a></li>
<li><a href="[[~6]]">Цены</a></li>
<li><a href="[[~7]]">Акции</a></li>
<li><a href="[[~8]]">Материалы</a></li>
<li><a href="[[~9]]">Акриловые вкладыши</a></li>
<li><a href="[[~10]]">Отзывы</a></li>
<li><a href="[[~11]]">Наш блог</a></li>
<li><a href="[[~12]]">Контакты</a></li>
    </ul> </div>  
',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта [[++site_url]]`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="[[~[[*id]]]]" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="footer">
              <div class="footbl">
                  <img src="../images/logo.png" />
                  <p>© 2005 - 2016 "Эколор38"</p></div>
              <div class="footbl">
      <a  href="http://ok.ru/ecolor" target="_blank"><img src="/images/odnoklassniki-logo.png" /></a>
      <a href="https://new.vk.com/ecolor38" target="_blank"><img src="/images/vk-social-logotype.png" /></a>
      <a href="https://www.instagram.com/vanna_irk/" target="_blank"><img src="/images/instagram-social-network-logo-of-photo-camera.png" /></a>
      <a href="https://www.youtube.com/channel/UCmEEi4v7It0r2rfLZj2HDUg" target="_blank"><img src="/images/youtube-logotype.png" /></a>
              </div>
              <div class="footbl">
     <p>г. Иркутск, ул. Лермонтова, 341/1</p>
     <p>г. Ангарск ул. Карла Маркса 75 офис №2</p>
     <p></p>
     <p>Почта: <a href="mailto:ecolor38@yandex.ru">ecolor38@yandex.ru</a></p>
     <p>Время работы: с 9:00 до 20:00</p>
              </div>
          </div>
           [[!FormIt?
   &hooks=`spam,email,FormItSaveForm,redirect`
   &emailTpl=`email_tpl4`
   &emailTo=`profitvan@yandex.ru`
   &emailFrom=`robot@ecolor38.ru`
   &emailSubject=`Письмо с сайта [[++site_url]]`
   &submitVar=`formpopup`
   &redirectTo=`31`
   &validate=`workemail:blank`
   &formName=`Обратный звонок`
   &formFields=`popemail,popname,popphone`
   &fieldNames=`popphone==Телефон,popname==Имя,popemail==Электронная почта`
]]
<!-- modal content -->
		<div id="basic-modal-content">
		  <form action="[[~[[*id]]]]" method="post">
     
        <h2>ЗАКАЖИТЕ ЗВОНОК</h2>
        <p>Оставьте ваши контакты<br/> и мы Вам перезвоним.</p>
        <input style="display:none;" name="workemail" value=""/>
        <input type="text" name="popname" placeholder="Введите ваше имя*" required title="Введите Ваше имя" value="[[!+fi.popname]]">
        <input type="text" name="popemail" placeholder="Введите Ваш Email*" required title="Введите email" value="[[!+fi.popemail]]">
        <input type="text" name="popphone" placeholder="Введите номер телефона*" required title="Введите номер телефона" value="[[!+fi.popphone]]">
        <input class="button" type="submit" name="formpopup" value="ОТПРАВИТЬ ЗАЯВКУ" >
        
        <p>Ваши данные в безопасности и не будут<br/> переданы 3-м лицам.</p>
        </form>
		</div>
		<!-- preload the images -->
		<div style=\'display:none\'>
			<img src=\'img/basic/x.png\' alt=\'\' />
		</div>
		  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link type=\'text/css\' href=\'css/basic.css\' rel=\'stylesheet\' media=\'screen\' />
    		<link type="text/css" href="site/css/jquery.fancybox-1.3.4.css" rel="stylesheet">
          <script type="text/javascript" src="site/js/jquery.js"></script>
		<script type="text/javascript" src="template/fancybox/jquery.fancybox.pack.js"></script>
		<script src="site/js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
		<script src="site/js/script.js" type="text/javascript"></script>
		
<script type=\'text/javascript\' src=\'js/jquery.simplemodal.js\'></script>
<script type=\'text/javascript\' src=\'js/basic.js\'></script>

<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_nav\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<script>
var h_hght = 120; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна
                 
$(function(){
 
    var elem = $(\'#top_navphone\');
    var top = $(this).scrollTop();
     
    if(top > h_hght){
        elem.css(\'top\', h_mrg);
    }           
     
    $(window).scroll(function(){
        top = $(this).scrollTop();
         
        if (top+h_mrg < h_hght) {
            elem.css(\'top\', (h_hght-top));
        } else {
            elem.css(\'top\', h_mrg);
        }
    });
 
});
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22784131 = new Ya.Metrika({
                    id:22784131,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22784131" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=e52343a26b7852a13151ea1f83217375" charset="UTF-8" async></script>
	',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'If' => 
      array (
        'fields' => 
        array (
          'id' => 13,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'If',
          'description' => 'Simple if (conditional) snippet',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'subject' => 
            array (
              'name' => 'subject',
              'desc' => 'The data being affected.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The data being affected.',
              'area_trans' => '',
            ),
            'operator' => 
            array (
              'name' => 'operator',
              'desc' => 'The type of conditional.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'EQ',
                  'text' => 'EQ',
                  'name' => 'EQ',
                ),
                1 => 
                array (
                  'value' => 'NEQ',
                  'text' => 'NEQ',
                  'name' => 'NEQ',
                ),
                2 => 
                array (
                  'value' => 'LT',
                  'text' => 'LT',
                  'name' => 'LT',
                ),
                3 => 
                array (
                  'value' => 'GT',
                  'text' => 'GT',
                  'name' => 'GT',
                ),
                4 => 
                array (
                  'value' => 'LTE',
                  'text' => 'LTE',
                  'name' => 'LTE',
                ),
                5 => 
                array (
                  'value' => 'GT',
                  'text' => 'GTE',
                  'name' => 'GTE',
                ),
                6 => 
                array (
                  'value' => 'EMPTY',
                  'text' => 'EMPTY',
                  'name' => 'EMPTY',
                ),
                7 => 
                array (
                  'value' => 'NOTEMPTY',
                  'text' => 'NOTEMPTY',
                  'name' => 'NOTEMPTY',
                ),
                8 => 
                array (
                  'value' => 'ISNULL',
                  'text' => 'ISNULL',
                  'name' => 'ISNULL',
                ),
                9 => 
                array (
                  'value' => 'inarray',
                  'text' => 'INARRAY',
                  'name' => 'INARRAY',
                ),
              ),
              'value' => 'EQ',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The type of conditional.',
              'area_trans' => '',
            ),
            'operand' => 
            array (
              'name' => 'operand',
              'desc' => 'When comparing to the subject, this is the data to compare to.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'When comparing to the subject, this is the data to compare to.',
              'area_trans' => '',
            ),
            'then' => 
            array (
              'name' => 'then',
              'desc' => 'If conditional was successful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was successful, output this.',
              'area_trans' => '',
            ),
            'else' => 
            array (
              'name' => 'else',
              'desc' => 'If conditional was unsuccessful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was unsuccessful, output this.',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'FormIt' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'FormIt',
          'description' => 'A dynamic form processing snippet.',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
          'locked' => false,
          'properties' => 
          array (
            'hooks' => 
            array (
              'name' => 'hooks',
              'desc' => 'prop_formit.hooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список хуков которые будут выполнятся по очереди после того как форма пройдёт проверку. Если какой-то из хуков вернёт false, то следующии хуки не будут выполнены. Хук также может быть именем сниппета, этот сниппет будет выполнен как хук.',
              'area_trans' => '',
            ),
            'preHooks' => 
            array (
              'name' => 'preHooks',
              'desc' => 'prop_formit.prehooks_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список хуков которые будут выполнятся по очереди после того как форма будет загружена. Если какой-то из хуков вернёт false, то следующие хуки не будут выполнены. Например: можно предварительно устанавливать значения полей формы с помощью $scriptProperties[`hook`]->fields[`fieldname`]. Хук также может быть именем сниппета, этот сниппет будет выполнен как хук.',
              'area_trans' => '',
            ),
            'submitVar' => 
            array (
              'name' => 'submitVar',
              'desc' => 'prop_formit.submitvar_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено значение, то обработка формы не начнётся пока  POST параметр с этим именем не будет передан.',
              'area_trans' => '',
            ),
            'validate' => 
            array (
              'name' => 'validate',
              'desc' => 'prop_formit.validate_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список полей для проверки, для каждого поля пишется имя:валидатор (т.е.: username:required,email:required). Валидаторы могут быть объединены через двоеточие, например email:email:required. Этот параметр может быть задан на нескольких строках.',
              'area_trans' => '',
            ),
            'errTpl' => 
            array (
              'name' => 'errTpl',
              'desc' => 'prop_formit.errtpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<span class="error">[[+error]]</span>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Шаблон сообщения об ошибке.',
              'area_trans' => '',
            ),
            'validationErrorMessage' => 
            array (
              'name' => 'validationErrorMessage',
              'desc' => 'prop_formit.validationerrormessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<p class="error">A form validation error occurred. Please check the values you have entered.</p>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'A general error message to set to a placeholder if validation fails. Can contain [[+errors]] if you want to display a list of all errors at the top.',
              'area_trans' => '',
            ),
            'validationErrorBulkTpl' => 
            array (
              'name' => 'validationErrorBulkTpl',
              'desc' => 'prop_formit.validationerrorbulktpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '<li>[[+error]]</li>',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'HTML tpl that is used for each individual error in the generic validation error message value.',
              'area_trans' => '',
            ),
            'trimValuesBeforeValidation' => 
            array (
              'name' => 'trimValuesBeforeValidation',
              'desc' => 'prop_formit.trimvaluesdeforevalidation_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Whether or not to trim spaces from the beginning and end of values before attempting validation. Defaults to true.',
              'area_trans' => '',
            ),
            'customValidators' => 
            array (
              'name' => 'customValidators',
              'desc' => 'prop_formit.customvalidators_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Разделённый запятыми список имён пользовательских валидаторов(сниппетов), которые вы планируете использовать в этой форме. Пользовательские валидаторы должны быть обязательно указаны в этом параметре, иначе они не будут работать.',
              'area_trans' => '',
            ),
            'clearFieldsOnSuccess' => 
            array (
              'name' => 'clearFieldsOnSuccess',
              'desc' => 'prop_formit.clearfieldsonsuccess_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено значение true, то поля формы будут очищатся после успешной отправки формы.',
              'area_trans' => '',
            ),
            'successMessage' => 
            array (
              'name' => 'successMessage',
              'desc' => 'prop_formit.successmessage_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Значение подстановщика для сообщения об успехе. Имя подстановщика устанавливается в параметре &successMessagePlaceholder, по умолчанию «fi.successMessage».',
              'area_trans' => '',
            ),
            'successMessagePlaceholder' => 
            array (
              'name' => 'successMessagePlaceholder',
              'desc' => 'prop_formit.successmessageplaceholder_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.successMessage',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Подстановщик для сообщения об успехе.',
              'area_trans' => '',
            ),
            'store' => 
            array (
              'name' => 'store',
              'desc' => 'prop_formit.store_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если установлено true,  данные переданные через форму будет сохранятcя в кэше, для дальнейшего их использования с помощью сниппета FormItRetriever.',
              'area_trans' => '',
            ),
            'placeholderPrefix' => 
            array (
              'name' => 'placeholderPrefix',
              'desc' => 'prop_formit.placeholderprefix_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'fi.',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Префикс который используется всеми подстановщиками установлеными FormIt для полей. По умолчанию «fi.»',
              'area_trans' => '',
            ),
            'storeTime' => 
            array (
              'name' => 'storeTime',
              'desc' => 'prop_formit.storetime_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '300',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если выбрано `запоминание` данных формы, то этот параметр указывает время(в секундах)  для хранения данных из отправленной формы. По умолчанию пять минут.',
              'area_trans' => '',
            ),
            'storeLocation' => 
            array (
              'name' => 'storeLocation',
              'desc' => 'prop_formit.storelocation_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'cache',
                  'text' => 'formit.opt_cache',
                  'name' => 'MODX Cache',
                ),
                1 => 
                array (
                  'value' => 'session',
                  'text' => 'formit.opt_session',
                  'name' => 'Session',
                ),
              ),
              'value' => 'cache',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If `store` is set to true, this specifies the cache location of the data from the form submission. Defaults to MODX cache.',
              'area_trans' => '',
            ),
            'allowFiles' => 
            array (
              'name' => 'allowFiles',
              'desc' => 'prop_formit.allowfiles_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If set to 0, will prevent files from being submitted on the form.',
              'area_trans' => '',
            ),
            'spamEmailFields' => 
            array (
              'name' => 'spamEmailFields',
              'desc' => 'prop_formit.spamemailfields_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «spam». Разделённый запятыми список полей содержащих адреса электронной почты для проверки на причастность к спаму.',
              'area_trans' => '',
            ),
            'spamCheckIp' => 
            array (
              'name' => 'spamCheckIp',
              'desc' => 'prop_formit.spamcheckip_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «spam». Если это параметр установлен в true, то будет проверяться ip-адресс отправителя формы на причастность к спаму.',
              'area_trans' => '',
            ),
            'recaptchaJs' => 
            array (
              'name' => 'recaptchaJs',
              'desc' => 'prop_formit.recaptchajs_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '{}',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «recaptcha».  JSON объект который содержит в себе  настройки для виджета reCaptcha.',
              'area_trans' => '',
            ),
            'recaptchaTheme' => 
            array (
              'name' => 'recaptchaTheme',
              'desc' => 'prop_formit.recaptchatheme_desc',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'red',
                  'text' => 'formit.opt_red',
                  'name' => 'Red',
                ),
                1 => 
                array (
                  'value' => 'white',
                  'text' => 'formit.opt_white',
                  'name' => 'White',
                ),
                2 => 
                array (
                  'value' => 'clean',
                  'text' => 'formit.opt_clean',
                  'name' => 'Clean',
                ),
                3 => 
                array (
                  'value' => 'blackglass',
                  'text' => 'formit.opt_blackglass',
                  'name' => 'Black Glass',
                ),
              ),
              'value' => 'clean',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «recaptcha». Тема оформления для виджета reCaptcha.',
              'area_trans' => '',
            ),
            'redirectTo' => 
            array (
              'name' => 'redirectTo',
              'desc' => 'prop_formit.redirectto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «redirect». В этом параметре надо указать идентификатор ресурса на который будет происходить редирект после успешной отправки формы.',
              'area_trans' => '',
            ),
            'redirectParams' => 
            array (
              'name' => 'redirectParams',
              'desc' => 'prop_formit.redirectparams_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => ' JSON array of parameters to pass to the redirect hook that will be passed when redirecting.',
              'area_trans' => '',
            ),
            'emailTo' => 
            array (
              'name' => 'emailTo',
              'desc' => 'prop_formit.emailto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Разделённый запятыми список адресов электронной почты на которые надо послать письмо.',
              'area_trans' => '',
            ),
            'emailToName' => 
            array (
              'name' => 'emailToName',
              'desc' => 'prop_formit.emailtoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailTo».',
              'area_trans' => '',
            ),
            'emailFrom' => 
            array (
              'name' => 'emailFrom',
              'desc' => 'prop_formit.emailfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Если этот параметр установлен, то он будет определять адрес электронной почты отправителя письма. Если не установлен, то сначала адрес электронной почты будет искаться в данных формы  в поле с именем «email», если это поле не будет найдено, то будет использоваться  адрес электронной почты из системной настройки «emailsender».',
              'area_trans' => '',
            ),
            'emailFromName' => 
            array (
              'name' => 'emailFromName',
              'desc' => 'prop_formit.emailfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Имя отправителя письма.',
              'area_trans' => '',
            ),
            'emailReplyTo' => 
            array (
              'name' => 'emailReplyTo',
              'desc' => 'prop_formit.emailreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Адрес электронной почты для ответа на письмо.',
              'area_trans' => '',
            ),
            'emailReplyToName' => 
            array (
              'name' => 'emailReplyToName',
              'desc' => 'prop_formit.emailreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Имя владельца адреса электронной почты который используется для ответа на письмо.',
              'area_trans' => '',
            ),
            'emailCC' => 
            array (
              'name' => 'emailCC',
              'desc' => 'prop_formit.emailcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Разделённый запятыми список адресов электронной почты на которые надо послать копию письма.',
              'area_trans' => '',
            ),
            'emailCCName' => 
            array (
              'name' => 'emailCCName',
              'desc' => 'prop_formit.emailccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailCC».',
              'area_trans' => '',
            ),
            'emailBCC' => 
            array (
              'name' => 'emailBCC',
              'desc' => 'prop_formit.emailbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email».  Разделённый запятыми список адресов  электронной почты на которые надо послать скрытую копию письма.',
              'area_trans' => '',
            ),
            'emailBCCName' => 
            array (
              'name' => 'emailBCCName',
              'desc' => 'prop_formit.emailbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailBCC».',
              'area_trans' => '',
            ),
            'emailReturnPath' => 
            array (
              'name' => 'emailReturnPath',
              'desc' => 'prop_formit.emailreturnpath_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Optional. If `email` is set as a hook, and this is set, will specify the Return-path: address for the email. If not set, will take the value of `emailFrom` property.',
              'area_trans' => '',
            ),
            'emailSubject' => 
            array (
              'name' => 'emailSubject',
              'desc' => 'prop_formit.emailsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». В этом параметре можно указать тему электронного письма.',
              'area_trans' => '',
            ),
            'emailUseFieldForSubject' => 
            array (
              'name' => 'emailUseFieldForSubject',
              'desc' => 'prop_formit.emailusefieldforsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Если поле «subject» используется в форме, и это параметр установлен в true,то содержимое этого поля будет использоваться как тема электронного письма.',
              'area_trans' => '',
            ),
            'emailHtml' => 
            array (
              'name' => 'emailHtml',
              'desc' => 'prop_formit.emailhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «email». Необязательный параметр. Этот параметр включает использование html разметки в электронном письме. По умолчанию включено.',
              'area_trans' => '',
            ),
            'emailConvertNewlines' => 
            array (
              'name' => 'emailConvertNewlines',
              'desc' => 'prop_formit.emailconvertnewlines_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'If true and emailHtml is set to 1, will convert newlines to BR tags in the email.',
              'area_trans' => '',
            ),
            'emailMultiWrapper' => 
            array (
              'name' => 'emailMultiWrapper',
              'desc' => 'prop_formit.emailmultiwrapper_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[+value]]',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Will wrap each item in a collection of fields sent via checkboxes/multi-selects. Defaults to just the value.',
              'area_trans' => '',
            ),
            'emailMultiSeparator' => 
            array (
              'name' => 'emailMultiSeparator',
              'desc' => 'prop_formit.emailmultiseparator_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'The default separator for collections of items sent through checkboxes/multi-selects. Defaults to a newline.',
              'area_trans' => '',
            ),
            'fiarTpl' => 
            array (
              'name' => 'fiarTpl',
              'desc' => 'prop_formit.fiartpl_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartpl_desc',
              'area_trans' => '',
            ),
            'fiarToField' => 
            array (
              'name' => 'fiarToField',
              'desc' => 'prop_formit.fiartofield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'email',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiartofield_desc',
              'area_trans' => '',
            ),
            'fiarSubject' => 
            array (
              'name' => 'fiarSubject',
              'desc' => 'prop_formit.fiarsubject_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '[[++site_name]] Auto-Responder',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarsubject_desc',
              'area_trans' => '',
            ),
            'fiarFrom' => 
            array (
              'name' => 'fiarFrom',
              'desc' => 'prop_formit.fiarfrom_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfrom_desc',
              'area_trans' => '',
            ),
            'fiarFromName' => 
            array (
              'name' => 'fiarFromName',
              'desc' => 'prop_formit.fiarfromname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarfromname_desc',
              'area_trans' => '',
            ),
            'fiarReplyTo' => 
            array (
              'name' => 'fiarReplyTo',
              'desc' => 'prop_formit.fiarreplyto_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplyto_desc',
              'area_trans' => '',
            ),
            'fiarReplyToName' => 
            array (
              'name' => 'fiarReplyToName',
              'desc' => 'prop_formit.fiarreplytoname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarreplytoname_desc',
              'area_trans' => '',
            ),
            'fiarCC' => 
            array (
              'name' => 'fiarCC',
              'desc' => 'prop_formit.fiarcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarcc_desc',
              'area_trans' => '',
            ),
            'fiarCCName' => 
            array (
              'name' => 'fiarCCName',
              'desc' => 'prop_fiar.fiarccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'Хук «FormItAutoResponder». Необязательный параметр.  Разделённый запятыми список имён владельцев адресов электронной почты указанных в параметре «emailCC».',
              'area_trans' => '',
            ),
            'fiarBCC' => 
            array (
              'name' => 'fiarBCC',
              'desc' => 'prop_formit.fiarbcc_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbcc_desc',
              'area_trans' => '',
            ),
            'fiarBCCName' => 
            array (
              'name' => 'fiarBCCName',
              'desc' => 'prop_formit.fiarbccname_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarbccname_desc',
              'area_trans' => '',
            ),
            'fiarHtml' => 
            array (
              'name' => 'fiarHtml',
              'desc' => 'prop_formit.fiarhtml_desc',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => true,
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.fiarhtml_desc',
              'area_trans' => '',
            ),
            'mathMinRange' => 
            array (
              'name' => 'mathMinRange',
              'desc' => 'prop_formit.mathminrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '10',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathminrange_desc',
              'area_trans' => '',
            ),
            'mathMaxRange' => 
            array (
              'name' => 'mathMaxRange',
              'desc' => 'prop_formit.mathmaxrange_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => '100',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathmaxrange_desc',
              'area_trans' => '',
            ),
            'mathField' => 
            array (
              'name' => 'mathField',
              'desc' => 'prop_formit.mathfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'math',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathfield_desc',
              'area_trans' => '',
            ),
            'mathOp1Field' => 
            array (
              'name' => 'mathOp1Field',
              'desc' => 'prop_formit.mathop1field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op1',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop1field_desc',
              'area_trans' => '',
            ),
            'mathOp2Field' => 
            array (
              'name' => 'mathOp2Field',
              'desc' => 'prop_formit.mathop2field_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'op2',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathop2field_desc',
              'area_trans' => '',
            ),
            'mathOperatorField' => 
            array (
              'name' => 'mathOperatorField',
              'desc' => 'prop_formit.mathoperatorfield_desc',
              'type' => 'textfield',
              'options' => '',
              'value' => 'operator',
              'lexicon' => 'formit:properties',
              'area' => '',
              'desc_trans' => 'prop_formit.mathoperatorfield_desc',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * FormIt
 *
 * Copyright 2009-2012 by Shaun McCormick <shaun@modx.com>
 *
 * FormIt is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * FormIt is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * FormIt; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package formit
 */
/**
 * FormIt
 *
 * A dynamic form processing Snippet for MODx Revolution.
 *
 * @var modX $modx
 * @var array $scriptProperties
 *
 * @package formit
 */

$modelPath = $modx->getOption(\'formit.core_path\', null, $modx->getOption(\'core_path\', null, MODX_CORE_PATH) . \'components/formit/\') . \'model/formit/\';
$modx->loadClass(\'FormIt\', $modelPath, true, true);

$fi = new FormIt($modx,$scriptProperties);
$fi->initialize($modx->context->get(\'key\'));
$fi->loadRequest();

$fields = $fi->request->prepare();
return $fi->request->handle($fields);',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Заголовок страницы',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'desc' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'desc',
          'caption' => 'Описание',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'keyw' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'keyw',
          'caption' => 'Ключевые слова',
          'description' => '',
          'editor_type' => 0,
          'category' => 3,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'maxLength' => '',
            'minLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => 'реставрация ванн в ангарске, реставрация ванн в усолье, покраска ванн, обновление эмали ванн, реставрация эмали ванны, покрытие ванн эмалью, восстановление ванны, восстановление эмали ванн, покраска ванны Иркутск, реставрация ванн, реставрация душевых кабин, ремонт акрилового поддона, фирма по реставрации ванн, наливная ванна, реставрация чугунной ванны',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);