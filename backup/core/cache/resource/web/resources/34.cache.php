<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 34,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Новая главная',
    'longtitle' => '',
    'description' => '',
    'alias' => 'novaya-glavnaya',
    'alias_visible' => 0,
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 7,
    'menuindex' => 22,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1558168371,
    'editedby' => 0,
    'editedon' => 0,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1558168371,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'novaya-glavnaya.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    '_content' => '<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>E-color 38</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link
            rel="stylesheet"
            type="text/css"
            media="screen"
            href="/assets/theme/style.css"
        />
        <script
            src="https://api-maps.yandex.ru/2.1/?apikey=23353558-5e85-4d3b-8759-f3722fec4460&lang=ru_RU"
            type="text/javascript"
        ></script>
    </head>
    <body>
        <header class="header">
            <div class="container">
                <div class="header-row">
                    <div class="header-logo">
                        <a href="/">
                            <img src="/assets/theme/images/header-logo.png" />
                        </a>
                    </div>
                    <div class="header-col-2 d-none d-md-block">
                        Надежное покрытие<br />
                        с гарантией <span class="bold">3-х лет</span>
                    </div>
                    <div class="header-col-3 d-none d-md-block header-address">
                        Работаем по Иркутску и области<br />
                        Продажа материала по всей России
                    </div>
                    <div class="header-contacts">
                        <div class="messangers">
                            <a href="#">
                                <img src="/assets/theme/images/viber.png" />
                            </a>
                            <a href="#">
                                <img src="/assets/theme/images/whatsapp.png" />
                            </a>
                            <a href="#">
                                <img src="/assets/theme/images/tg.png" />
                            </a>
                        </div>
                        <div class="header-phone phone">
                            <a href="tel:+79027631386">8-902-763-13-86</a>
                        </div>
                        <div class="header-callback  d-none d-md-block">
                            <a href="#">Обратный звонок</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-menu">
                <div class="container">
                    <div class="header-menu-row">
                        <div class="header-menu-toggler d-md-none"></div>
                        <ul class="header-menu-list">
                            <li class="header-menu-services">
                                <a href="#">Услуги</a>
                            </li>
                            <li><a href="#">Цены</a></li>
                            <li><a href="#">Экраны</a></li>
                            <li><a href="#">Акции</a></li>
                            <li><a href="#">Материалы</a></li>
                            <li><a href="#">Акриловые вкладыши</a></li>
                            <li><a href="#">Отзывы</a></li>
                            <li><a href="#">Наш блог</a></li>
                            <li><a href="#">О нас</a></li>
                            <li><a href="#">Контакты</a></li>
                        </ul>
                        <div class="header-cart">
                            <div id="msMiniCart" class="">
                                <div class="not_empty">
                                    <a href="#">
                                        <img
                                            src="/assets/theme/images/cart.png"
                                        />
                                        <span class="cart-badge ms2_total_count"
                                            >2</span
                                        >
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <section class="index-header">
            <div class="container">
                <div class="index-header-top">
                    <div class="inder-header-title">
                        Сделаем реставрацию ванны<br />
                        «по-человечески»
                    </div>
                    <div class="inder-header-subtitle">
                        Качественно, без навязывания услуг <br />
                        и неожиданных доплат Такую, чтобы <br />
                        надолго, с реальной гарантией.
                    </div>
                    <div class="inder-header-image">
                        <img src="/assets/theme/images/bath.png" />
                    </div>
                    <div class="index-header-btns">
                        <a href="#" class="btn btn-lg btn-dark"
                            >Заказать со скидкой</a
                        >
                        <a href="#" class="btn btn-lg btn-dark-outline"
                            >Вызвать мастера</a
                        >
                    </div>
                </div>
                <div class="index-header-list">
                    <div class="item">
                        <div class="image">
                            <img
                                src="/assets/theme/images/index-header-1.png"
                            />
                        </div>
                        <div class="caption">Официальный договор</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img
                                src="/assets/theme/images/index-header-2.png"
                            />
                        </div>
                        <div class="caption">
                            Полностью безопасно для здоровья
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img
                                src="/assets/theme/images/index-header-3.png"
                            />
                        </div>
                        <div class="caption">Срок службы До 20 лет</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img
                                src="/assets/theme/images/index-header-4.png"
                            />
                        </div>
                        <div class="caption">Работа мастера 3 часа</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img
                                src="/assets/theme/images/index-header-5.png"
                            />
                        </div>
                        <div class="caption">Застывание 24 часа</div>
                    </div>
                </div>
            </div>
        </section>

        <section class="services">
            <div class="container">
                <h3 class="h3 text-center">Наши цены</h3>
                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="service">
                            <div class="image">
                                <img src="/assets/theme/images/service-1.png" />
                            </div>
                            <div class="title">
                                Реставрация ванны Стакрилом
                            </div>
                            <div class="price">4 000 руб</div>
                            <div class="caption">
                                Оптимальный вариант, если Вы планируете сделать
                                разумный ремонт без демонтажа ванны с изношенной
                                поверхностью.
                            </div>
                            <ul class="features">
                                <li>Без демонтажа ванны</li>
                                <li>Срок службы до 15 лет</li>
                                <li>Толщина покрытия 3 - 5 мм</li>
                                <li>При покрытии минимальный запах.</li>
                            </ul>
                            <div class="circs">
                                <div class="work-time">
                                    Время на работу - 1,5 - 2 часа
                                </div>
                                <div class="dry-time">
                                    Время высыхания - 24 - 36 часов
                                </div>
                                <div class="warranty">
                                    Гарантии на ванну нет
                                </div>
                            </div>
                            <div class="service-btns">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >Заказать
                                </a>
                            </div>

                            <div class="service-video-link">
                                <a class="link" href="#">Смотреть видео</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="service">
                            <div class="image">
                                <img src="/assets/theme/images/service-2.png" />
                            </div>
                            <div class="title">
                                Наливная ванна "Акрил Люкс"
                            </div>
                            <div class="price">4 990 руб</div>
                            <div class="caption">
                                Наливное акриловое покрытие с более быстрым
                                сроком застывания и сроком службы. Высокая
                                адгезия.
                            </div>
                            <ul class="features">
                                <li>Без демонтажа ванны</li>
                                <li>Срок службы до 20 лет</li>
                                <li>Толщина покрытия 4 - 5 мм</li>
                                <li>При покрытии акрилом нет запаха.</li>
                            </ul>
                            <div class="circs">
                                <div class="work-time">
                                    Время на работу - 1,5 - 2 часа
                                </div>
                                <div class="dry-time">
                                    Время высыхания - 24 - 36 часов
                                </div>
                                <div class="warranty">
                                    Гарантии на ванну нет
                                </div>
                            </div>
                            <div class="service-btns">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >Заказать
                                </a>
                            </div>

                            <div class="service-video-link">
                                <a class="link" href="#">Смотреть видео</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="service">
                            <div class="image">
                                <img src="/assets/theme/images/service-3.png" />
                            </div>
                            <div class="title">
                                Ремонт квартир/ванн
                            </div>
                            <div class="price">10 500 руб</div>
                            <div class="caption">
                                Акриловый вкладыш ванну Это новая акриловая
                                ванна на базе вашей старой чугунной! Толщина
                                6мм. Можно сразу пользоватся!
                            </div>
                            <ul class="features">
                                <li>Без демонтажа ванны</li>
                                <li>Срок службы 20 лет</li>
                                <li>Толщина стенки вкладыша 6 мм</li>
                                <li>При установке нет запаха.</li>
                            </ul>
                            <div class="circs">
                                <div class="work-time">
                                    Время на работу - 1,5 - 2 часа
                                </div>
                                <div class="dry-time">
                                    Время высыхания - 24 - 36 часов
                                </div>
                                <div class="warranty">
                                    Гарантии на ванну нет
                                </div>
                            </div>

                            <div class="service-btns">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >Заказать
                                </a>
                            </div>

                            <div class="service-video-link">
                                <a class="link" href="#">Смотреть видео</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="service">
                            <div class="image">
                                <img src="/assets/theme/images/service-4.png" />
                            </div>
                            <div class="title">
                                Ремонт квартир/ванн
                            </div>
                            <div class="price">от 12 500 руб</div>
                            <div class="caption">
                                Самый быстрый и не затратный способ освежить
                                вашу квартиру, изменить ее дизайн, сделать более
                                уютной и современной.
                            </div>
                            <ul class="features">
                                <li>Без демонтажа ванны</li>
                                <li>Срок службы до 3 лет</li>
                                <li>Толщина покрытия от 0,3 мм до 5 мм</li>
                                <li>При эмалировке есть запах.</li>
                            </ul>

                            <div class="circs">
                                <div class="work-time">
                                    Время на работу - 1,5 - 2 часа
                                </div>
                                <div class="dry-time">
                                    Время высыхания - 24 - 36 часов
                                </div>
                                <div class="warranty">
                                    Гарантии на ванну нет
                                </div>
                            </div>

                            <div class="service-btns">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >Заказать
                                </a>
                            </div>

                            <div class="service-video-link">
                                <a class="link" href="#">Смотреть видео</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="about blue-bg">
            <div class="container">
                <h3 class="h3 text-center">Немного о нас</h3>
                <div class="row">
                    <div class="col-md-8 col-xl-7">
                        <ul>
                            <li>
                                Фирма по реставрации ванн «Эколор38» была
                                основана в уже далеком 2010 году в городе
                                Иркутске, где и продолжает работать по сей день.
                                Богатый опыт и большие возможности позволило
                                именно этой компании стать официальным дилером
                                компании «Эколор» и компании "Экованна" в своем
                                регионе.
                            </li>
                            <li>
                                В «Эколор38» о ремонте знают всё! Использование
                                передовых технологий, качественных материалов и
                                наработанный с годами опыт позволили компании
                                завоевать доверие потребителей. Сегодня компания
                                «Эколор38» является одним из лидеров на рынке и
                                выполняет тысячи заказов ежегодно.
                            </li>
                            <li>
                                Компания «Эколор38» развивается и никогда не
                                стоит на месте. Постоянно разрабатываются новые
                                направления, подыскиваются оптимальные решения
                                для оборудования и качественного ремонта ванных
                                комнат. Цены как на произведение реставрационных
                                работ, так и наш материал остаются стабильно
                                низкими, что позволяет расширять рынки сбыта и
                                находить партнеров.
                            </li>
                        </ul>
                    </div>
                    <div class="offset-xl-1 col-md-4 about-images-col">
                        <div class="h5">Мы реставрируем</div>
                        <div class="row">
                            <div class="col-6">
                                <div class="item about-item-1">
                                    <div class="image">
                                        <img
                                            src="/assets/theme/images/about-1.png"
                                            alt=""
                                        />
                                    </div>
                                    <div class="caption">Ванны</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="item about-item-2">
                                    <div class="image">
                                        <img
                                            src="/assets/theme/images/about-2.png"
                                            alt=""
                                        />
                                    </div>
                                    <div class="caption">Джакузи</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="item about-item-3">
                                    <div class="image">
                                        <img
                                            src="/assets/theme/images/about-3.png"
                                            alt=""
                                        />
                                    </div>
                                    <div class="caption">Душевые кабины</div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="item about-item-4">
                                    <div class="image">
                                        <img
                                            src="/assets/theme/images/about-4.png"
                                            alt=""
                                        />
                                    </div>
                                    <div class="caption">Бассейны</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="reviews">
            <div class="container">
                <h3 class="h3 text-center">Отзывы наших клиентов</h3>
                <h4 class="h4 text-center">
                    Посмотрите, какие результаты уже получили наши клиенты
                </h4>
                <div class="reviews-row row">
                    <div class="col-md-8">
                        <a href="#" class="review">
                            <img src="/assets/theme/images/review.png" />
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="review">
                            <img src="/assets/theme/images/review.png" />
                        </a>
                        <a href="#" class="review">
                            <img src="/assets/theme/images/review.png" />
                        </a>
                    </div>
                </div>
                <div class="reviews-more text-center">
                    <a href="#" class="btn btn-lg btn-dark btn-more"
                        >Больше отзывов</a
                    >
                </div>
            </div>
        </section>

        <section class="results blue-bg">
            <div class="container">
                <h3 class="h3 text-center">Результаты нашей работы</h3>
                <div class="results-slider">
                    <div class="results-slide">
                        <div class="cocoen">
                            <img src="/assets/theme/images/result-1.jpg" />
                            <img src="/assets/theme/images/result-2.jpg" />
                        </div>
                        <div class="title">
                            Реставрация ванны Стакрилом
                        </div>
                        <div class="subtitle">Срок 2 дня</div>
                        <div class="result-btn">
                            <a href="#" class="btn btn-dark">Заказать</a>
                        </div>
                    </div>
                    <div class="results-slide">
                        <div class="cocoen">
                            <img src="/assets/theme/images/result-3.jpg" />
                            <img src="/assets/theme/images/result-4.jpg" />
                        </div>
                        <div class="title">
                            Реставрация ванны Стакрилом
                        </div>
                        <div class="subtitle">Срок 2 дня</div>
                        <div class="result-btn">
                            <a href="#" class="btn btn-dark">Заказать</a>
                        </div>
                    </div>
                    <div class="results-slide">
                        <div class="cocoen">
                            <img src="/assets/theme/images/result-5.jpg" />
                            <img src="/assets/theme/images/result-6.jpg" />
                        </div>
                        <div class="title">
                            Реставрация ванны Стакрилом
                        </div>
                        <div class="subtitle">Срок 2 дня</div>
                        <div class="result-btn">
                            <a href="#" class="btn btn-dark">Заказать</a>
                        </div>
                    </div>
                    <div class="results-slide">
                        <div class="cocoen">
                            <img src="/assets/theme/images/result-7.jpg" />
                            <img src="/assets/theme/images/result-8.jpg" />
                        </div>
                        <div class="title">
                            Реставрация ванны Стакрилом
                        </div>
                        <div class="subtitle">Срок 2 дня</div>
                        <div class="result-btn">
                            <a href="#" class="btn btn-dark">Заказать</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="products">
            <div class="container">
                <h3 class="h3">
                    Мы продаем материалы недорого
                    <div class="products-slider-btns"></div>
                    <div class="products-slider-more">
                        <a href="#">Смотреть все</a>
                    </div>
                </h3>
                <div class="products-slider">
                    <div class="product">
                        <div class="item">
                            <div class="image">
                                <img
                                    src="/assets/theme/images/materials-1.png"
                                />
                            </div>
                            <div class="title">
                                Эмаль "Жидкий акрил Люкс"+отвердитель
                            </div>
                            <div class="circs">
                                <div class="volume">
                                    2,2 кг. (для ванны 1,2 м)
                                </div>
                                <div class="dry-time">
                                    Застывание 24 часа
                                </div>
                                <div class="warranty">
                                    Срок службы 15 лет
                                </div>
                            </div>

                            <div class="caption">
                                В комплект входит:
                            </div>
                            <ul class="features">
                                <li>
                                    Жидкий акрил "Люкс" 1,2 м (24 часа) основа -
                                    2,2 кг
                                </li>
                                <li>Отвердитель - 1 шт.</li>
                                <li>
                                    Подробная инструкция для самостоятельной
                                    реставрации ванны
                                </li>
                            </ul>
                            <div class="price">2049 руб</div>
                            <div class="product-buy-btn">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >В корзину
                                </a>
                            </div>

                            <div class="product-more">
                                <a class="link" href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="item">
                            <div class="image">
                                <img
                                    src="/assets/theme/images/materials-2.png"
                                />
                            </div>
                            <div class="title">
                                Эмаль "Жидкий акрил Люкс"+отвердитель
                            </div>
                            <div class="circs">
                                <div class="volume">
                                    2,2 кг. (для ванны 1,2 м)
                                </div>
                                <div class="dry-time">
                                    Застывание 24 часа
                                </div>
                                <div class="warranty">
                                    Срок службы 15 лет
                                </div>
                            </div>

                            <div class="caption">
                                В комплект входит:
                            </div>
                            <ul class="features">
                                <li>
                                    Жидкий акрил "Люкс" 1,2 м (24 часа) основа -
                                    2,2 кг
                                </li>
                                <li>Отвердитель - 1 шт.</li>
                                <li>
                                    Подробная инструкция для самостоятельной
                                    реставрации ванны
                                </li>
                            </ul>
                            <div class="price">2049 руб</div>
                            <div class="product-buy-btn">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >В корзину
                                </a>
                            </div>

                            <div class="product-more">
                                <a class="link" href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="item">
                            <div class="image">
                                <img
                                    src="/assets/theme/images/materials-3.png"
                                />
                            </div>
                            <div class="title">
                                Эмаль "Жидкий акрил Люкс"+отвердитель
                            </div>
                            <div class="circs">
                                <div class="volume">
                                    2,2 кг. (для ванны 1,2 м)
                                </div>
                                <div class="dry-time">
                                    Застывание 24 часа
                                </div>
                                <div class="warranty">
                                    Срок службы 15 лет
                                </div>
                            </div>

                            <div class="caption">
                                В комплект входит:
                            </div>
                            <ul class="features">
                                <li>
                                    Жидкий акрил "Люкс" 1,2 м (24 часа) основа -
                                    2,2 кг
                                </li>
                                <li>Отвердитель - 1 шт.</li>
                                <li>
                                    Подробная инструкция для самостоятельной
                                    реставрации ванны
                                </li>
                            </ul>
                            <div class="price">2049 руб</div>
                            <div class="product-buy-btn">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >В корзину
                                </a>
                            </div>

                            <div class="product-more">
                                <a class="link" href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="item">
                            <div class="image">
                                <img
                                    src="/assets/theme/images/materials-1.png"
                                />
                            </div>
                            <div class="title">
                                Эмаль "Жидкий акрил Люкс"+отвердитель
                            </div>
                            <div class="circs">
                                <div class="volume">
                                    2,2 кг. (для ванны 1,2 м)
                                </div>
                                <div class="dry-time">
                                    Застывание 24 часа
                                </div>
                                <div class="warranty">
                                    Срок службы 15 лет
                                </div>
                            </div>

                            <div class="caption">
                                В комплект входит:
                            </div>
                            <ul class="features">
                                <li>
                                    Жидкий акрил "Люкс" 1,2 м (24 часа) основа -
                                    2,2 кг
                                </li>
                                <li>Отвердитель - 1 шт.</li>
                                <li>
                                    Подробная инструкция для самостоятельной
                                    реставрации ванны
                                </li>
                            </ul>
                            <div class="price">4 000 руб</div>
                            <div class="product-buy-btn">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >В корзину
                                </a>
                            </div>

                            <div class="product-more">
                                <a class="link" href="#">Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="product">
                        <div class="item">
                            <div class="image">
                                <img src="/assets/theme/images/service-1.png" />
                            </div>
                            <div class="title">
                                Реставрация ванны Стакрилом
                            </div>
                            <div class="price">4 000 руб</div>
                            <div class="caption">
                                Оптимальный вариант, если Вы планируете сделать
                                разумный ремонт без демонтажа ванны с изношенной
                                поверхностью.
                            </div>
                            <ul class="features">
                                <li>Без демонтажа ванны</li>
                                <li>Срок службы до 15 лет</li>
                                <li>Толщина покрытия 3 - 5 мм</li>
                                <li>При покрытии минимальный запах.</li>
                            </ul>

                            <div class="circs">
                                <div class="work-time">
                                    Время на работу - 1,5 - 2 часа
                                </div>
                                <div class="dry-time">
                                    Время высыхания - 24 - 36 часов
                                </div>
                                <div class="warranty">
                                    Гарантии на ванну нет
                                </div>
                            </div>

                            <div class="product-buy-btn">
                                <a href="#" class="btn btn-lg btn-dark-outline"
                                    >Заказать
                                </a>
                            </div>

                            <div class="product-more">
                                <a class="link" href="#">Смотреть видео</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="new-bath blue-bg">
            <div class="container">
                <h3 class="h3">Новая ванна за 3000 руб</h3>
                <div class="new-bath-image">
                    <div class="green">Реставрация - 3000 руб</div>
                    <div class="red">Новая ванна - 10000 руб</div>
                </div>
                <p>
                    Восстановление эмали ванн чаще всего занимает меньше
                    времени, чем установка новой сантехники. Исключение
                    составляет лишь устройство наливной ванны – полностью готова
                    к эксплуатации она будет через 36 часов после окончания
                    работ.
                </p>
                <p>
                    Но и в этом случае за восстановление ванн цена говорит свое
                    веское слово: это выгодно. Кроме минимизации затрат на
                    покупку есть и скрытые на первый взгляд выгоды. Вам не
                    придется нанимать машину для вывоза старой емкости, не
                    понадобится убирать строительный мусор после демонтажа.
                </p>
                <p>
                    Восстановление чугунных ванн – особая тема. Эмалевое
                    покрытие, даже самое качественное, даже при бережном уходе,
                    использовании прекрасно очищенной воды служит недолго. Но
                    сама по себе такая ванна может эксплуатироваться сотни лет.
                    Владельцам чугунных ванн Пенза Ванна рекомендует выбрать
                    восстановление ванн акрилом – это означает еще 10-15 лет
                    беспроблемной службы с улучшенными по теплосбережению
                    характеристиками.
                </p>
            </div>
        </section>

        <section class="diplomas">
            <h3 class="h3 text-center">Наши сертификаты</h3>
            <div class="diplomas-slider">
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
                <div class="diplomas-item">
                    <img src="/assets/theme/images/diploma.png" />
                </div>
            </div>
        </section>

        <section class="steps blue-bg">
            <div class="container">
                <h3 class="h3 text-center">Как мы работаем</h3>
                <div class="steps-list">
                    <div class="item">
                        <div class="image">
                            <img src="/assets/theme/images/steps-1.png" />
                        </div>
                        <div class="caption">Оставляете заявку</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="/assets/theme/images/steps-2.png" />
                        </div>
                        <div class="caption">Замер</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="/assets/theme/images/steps-3.png" />
                        </div>
                        <div class="caption">Подписание договора</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="/assets/theme/images/steps-4.png" />
                        </div>
                        <div class="caption">Реставрация</div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img src="/assets/theme/images/steps-5.png" />
                        </div>
                        <div class="caption">Оплата</div>
                    </div>
                </div>
                <div class="steps-subtext text-center">Этапы работы</div>
            </div>
        </section>

        <section class="questions">
            <div class="container">
                <h3 class="h3 text-center">Вопросы и ответы</h3>
                <div class="question-accordion">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="questions-list">
                                <a href="#" class="active"
                                    >Что если имеется пробоина в ванне</a
                                >
                                <a href="#" class=""
                                    >Что если имеется пробоина в ванне</a
                                >
                                <a href="#" class=""
                                    >Что если имеется пробоина в ванне</a
                                >
                                <a href="#" class=""
                                    >Что если имеется пробоина в ванне</a
                                >
                                <a href="#" class=""
                                    >Что если имеется пробоина в ванне</a
                                >
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="questions-texts">
                                <div class="item active">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                </div>

                                <div class="item">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                </div>

                                <div class="item">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                </div>

                                <div class="item">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                </div>

                                <div class="item">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur
                                        adipisicing elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna
                                        aliqua. Ut enim ad minim veniam, quis
                                        nostrud exercitation ullamco laboris
                                        nisi ut aliquip ex ea commodo consequat.
                                        Duis aute irure dolor in reprehenderit
                                        in voluptate velit esse cillum dolore eu
                                        fugiat nulla pariatur.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="map-form">
            <div class="map">
                <div id="map" style="width: 100%; height: 640px"></div>
                <script type="text/javascript">
                    ymaps.ready(function() {
                        var map = new ymaps.Map("map", {
                            center: [55.76, 37.64],
                            zoom: 10
                        });

                        if (map) {
                            ymaps.modules.require(
                                ["Placemark", "Circle"],
                                function(Placemark, Circle) {
                                    var placemark = new Placemark([
                                        55.37,
                                        35.45
                                    ]);
                                }
                            );
                        }
                    });
                </script>
            </div>
            <div class="form-container">
                <div class="form">
                    <form action="">
                        <div class="title">
                            Заполните форму
                        </div>
                        <div class="subtitle">
                            и получите скидку - 10%
                        </div>
                        <div class="caption">
                            Наш менеджер свяжется с Вами в течении
                            <strong>15 минут</strong> для расчета и согласования
                            удобной даты
                        </div>
                        <div class="input-group">
                            <input type="text" name="name" />
                        </div>
                        <div class="input-group">
                            <input type="text" name="phone" />
                        </div>
                        <div class="form-btn">
                            <button class="btn btn-lg btn-dark">
                                Заказать со скидкой
                            </button>
                        </div>
                        <div class="subtext">
                            Ваши данные в безопасности и не будут переданы 3-м
                            лицам.
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <footer class="footer">
            <div class="container">
                <div class="footer-row">
                    <div class="footer-logo-col">
                        <div class="footer-logo">
                            <a href="/">
                                <img
                                    src="/assets/theme/images/footer-logo.png"
                                    alt=""
                            /></a>
                        </div>
                        <div class="subtext">
                            производственно-ремонтная компания
                        </div>
                        <div class="footer-menu">
                            <a href="#">О нас</a>
                            <a href="#">Услуги</a>
                            <a href="#">Цены</a>
                        </div>
                    </div>
                    <div class="footer-address-col">
                        <div>г. Иркутск, ул. Лермонтова, 341/1</div>
                        <div>г. Ангарск ул. Карла Маркса 74А офис №108</div>
                    </div>
                    <div class="footer-mail-col">
                        <a href="mailto:ecolor38@yandex.ru"
                            >ecolor38@yandex.ru</a
                        >
                    </div>
                    <div class="footer-socials-col">
                        <a href="#">
                            <img
                                src="/assets/theme/images/footer-inst.png"
                                alt=""
                            />
                        </a>
                        <a href="#">
                            <img
                                src="/assets/theme/images/footer-vk.png"
                                alt=""
                            />
                        </a>
                        <a href="#">
                            <img
                                src="/assets/theme/images/footer-ok.png"
                                alt=""
                            />
                        </a>
                        <a href="#">
                            <img
                                src="/assets/theme/images/footer-youtube.png"
                                alt=""
                            />
                        </a>
                    </div>
                    <div class="footer-callback-col">
                        <div class="footer-phone phone">
                            <a href="tel:+79027631386">8-902-763-13-86</a>
                        </div>
                        <div class="footer-callback  d-none d-md-block">
                            <a href="#">Обратный звонок</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="subfooter">
                <div class="container">
                    <span>ИНН: 3849069576</span> <span>ОГРН 1183850017647</span>
                </div>
            </div>
        </footer>

        <script src="/assets/theme/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/assets/theme/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js"></script>
        <script src="/assets/theme/node_modules/slick-carousel/slick/slick.min.js"></script>
        <script src="/assets/theme/node_modules/cocoen/dist/js/cocoen.min.js"></script>
        <script src="/assets/theme/node_modules/cocoen/dist/js/cocoen-jquery.min.js"></script>
        <script src="/assets/theme/app.js"></script>
    </body>
</html>
',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
    ),
    'modSnippet' => 
    array (
    ),
    'modTemplateVar' => 
    array (
    ),
  ),
);