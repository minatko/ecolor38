function sliders() {
    $(".products-slider").slick({
        slidesToShow: 3,
        appendArrows: ".products-slider-btns",
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    appendArrows: $(".products-slider")
                }
            }
        ]
    });
    $(".material-plus-slider").slick({
        slidesToShow: 4
    });
    $(".ekrany-slider").slick({
        slidesToShow: 7
    });

    $(".related-slider").slick({
        slidesToShow: 4
    });

    $(".diplomas-slider").slick({
        slidesToShow: 5,
        centerMode: true,
        centerPadding: "40px"
    });
}

function servicesHover() {
    var services = $(".services");
    var service = $(".service");

    function serviceHoverIn(e) {
        $(this)
            .addClass("hover")
            .find(".circs")
            .show();
    }

    function serviceHoverOut(e) {
        $(this)
            .removeClass("hover")
            .find(".circs")
            .hide();
    }

    service.hover(serviceHoverIn, serviceHoverOut);
}

function accordion() {
    var accordion = $(".question-accordion");
    var links = $(".questions-list > a");
    var texts = $(".questions-texts > .item");

    links.click(function(e) {
        e.preventDefault();

        var linkIndex = links.index(this);

        links.removeClass("active");
        $(this).addClass("active");

        texts.removeClass("active");
        texts.eq(linkIndex).addClass("active");
    });
}

function results() {
    $(".cocoen").cocoen();

    $(".results-slider").slick({
        slidesToShow: 1,
        swipe: false,
        touchMove: false
    });
}

$(function() {
    sliders();
    servicesHover();
    accordion();
    results();
});
