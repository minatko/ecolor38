$(document).ready(function(){
	$("a[rel^='flowers']").fancybox({
		"cyclic": true,
		"speedIn": 700,
		"speedOut": 500,
		"overlayColor": "#d6d6d6",
		"overlayOpacity": 0.4,
		"transitionIn": "elastic",
		"transitionOut": "elastic"
	
  });
});